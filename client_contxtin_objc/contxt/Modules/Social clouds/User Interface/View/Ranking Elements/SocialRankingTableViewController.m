//
//  SocialRankingTableViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 3/10/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "SocialRankingTableViewController.h"
#import "RankingItemProtocol.h"

@interface SocialRankingTableViewController ()

@property (strong, nonatomic)NSFetchedResultsController *fetchedResultsController;

@end

NSInteger const socialTableViewRankingRowHeight = 120;

@implementation SocialRankingTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)setTableView:(UITableView *)tableView{
    _tableView = tableView;
    _tableView.rowHeight = socialTableViewRankingRowHeight;
    [self loadFetchedResultController];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView reloadData];
}

- (void)loadFetchedResultController
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    fetchRequest.entity=[NSEntityDescription entityForName:@"Ranking" inManagedObjectContext:[NSManagedObjectContext MR_defaultContext]];
    fetchRequest.predicate=[self defaultPredicate];
    fetchRequest.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES selector:@selector(caseInsensitiveCompare:)]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[NSManagedObjectContext MR_defaultContext] sectionNameKeyPath:nil cacheName:nil];
    self.fetchedResultsController.delegate=self;
    NSError *error=nil;
    [self.fetchedResultsController performFetch:&error];
    if (error) {
        NSLog(@"%@", error);
    }
}



- (NSPredicate *)defaultPredicate{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cloud = %@", self.cloudPresented];
    return predicate;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count=[[self.fetchedResultsController.sections objectAtIndex:0] numberOfObjects];
    return count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    id<RankingItemProtocol> item = (id<RankingItemProtocol>)[self.fetchedResultsController objectAtIndexPath:indexPath];
    UITableViewCell *cell = [item.cellDrawer cellForTableView:tableView atIndexPath:indexPath];
    
    [item.cellDrawer drawCell:cell withItem:item];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id<RankingItemProtocol> item = (id<RankingItemProtocol>)[self.fetchedResultsController objectAtIndexPath:indexPath];
    [self.router loadSelectedItemContent:[item contentsViewControllerWithSessionManager:self.sessionManager router:self.router]];
}


#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableViewRowAnimation animation = UITableViewRowAnimationFade;
    NSIndexPath *changeIndexPath=indexPath;
    NSIndexPath *newChangeIndexPath=newIndexPath;
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newChangeIndexPath] withRowAnimation:animation];
            break;
        case NSFetchedResultsChangeMove:
            if ([self.tableView respondsToSelector:@selector(moveRowAtIndexPath:toIndexPath:)]) {
                [self.tableView moveRowAtIndexPath:changeIndexPath toIndexPath:newChangeIndexPath];
            } else {
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:changeIndexPath] withRowAnimation:animation];
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:newChangeIndexPath.section] withRowAnimation:animation];
            }
            break;
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:changeIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:changeIndexPath] withRowAnimation:animation];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    NSIndexSet *indexSetWithSectionIndex = [NSIndexSet indexSetWithIndex:sectionIndex];
    
    UITableViewRowAnimation animation = UITableViewRowAnimationFade;
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:indexSetWithSectionIndex withRowAnimation:animation];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:indexSetWithSectionIndex withRowAnimation:animation];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.tableView endUpdates];
}

@end
