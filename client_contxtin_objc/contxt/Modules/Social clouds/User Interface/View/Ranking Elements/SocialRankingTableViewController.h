//
//  SocialRankingTableViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 3/10/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"
#import "Cloud.h"
#import "CloudsWireFrame.h"

@interface SocialRankingTableViewController : UIViewController<NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) Cloud *cloudPresented;
@property (strong, nonatomic) CloudsWireFrame *router;

- (void) initTableView;

@end
