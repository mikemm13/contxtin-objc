//
//  RankingItemProtocol.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocialRankingCellDrawerProtocol.h"
#import "SessionManager.h"

@protocol SocialRankingItemProtocol <NSObject>
@property (copy,nonatomic,readonly) NSString *itemTitle;
@property (strong,nonatomic,readonly) id<SocialRankingCellDrawerProtocol> cellDrawer;
- (UIViewController *)contentsViewControllerWithSessionManager:(SessionManager *)sessionManager;
@end
