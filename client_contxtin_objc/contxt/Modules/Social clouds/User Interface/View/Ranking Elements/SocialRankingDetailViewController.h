//
//  RankingDetailViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 24/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ranking.h"
#import "SessionManager.h"

@interface SocialRankingDetailViewController : UIViewController

- (instancetype)initWithNibName:(NSString*)nibNameOrNil ranking:(Ranking *)rankingElement sessionManager:(SessionManager *)sessionManager;

@end
