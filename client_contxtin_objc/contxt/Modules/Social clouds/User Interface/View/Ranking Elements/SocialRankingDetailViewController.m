//
//  RankingDetailViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 24/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "SocialRankingDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SocialRankingDetailViewController ()
@property (strong, nonatomic) Ranking *rankingElement;
@property (strong, nonatomic) SessionManager *sessionManager;
@property (weak, nonatomic) IBOutlet UILabel *rankingTitle;

@property (weak, nonatomic) IBOutlet UILabel *sourceName;
@property (weak, nonatomic) IBOutlet UILabel *publishDate;
@property (weak, nonatomic) IBOutlet UILabel *readingTime;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *cleanBody;
@property (strong, nonatomic) IBOutlet UIWebView *cloudWebView;

@end

@implementation SocialRankingDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil ranking:(Ranking *)rankingElement sessionManager:(SessionManager *)sessionManager
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        _rankingElement = rankingElement;
        _sessionManager = sessionManager;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.rankingTitle.text = self.rankingElement.title;
    self.sourceName.text = self.rankingElement.sourceName;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMMM yyyy, hh:mm"];
    
    self.publishDate.text = [formatter stringFromDate:self.rankingElement.publishDate];
    
    self.readingTime.text = self.rankingElement.readingTime;
    self.cleanBody.text = self.rankingElement.body;
    NSURL *url = [NSURL URLWithString:self.rankingElement.image];
    [self.image sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onPressedVisitWebsite:(id)sender {
    self.cloudWebView = [[UIWebView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    NSURL *url = [NSURL URLWithString:self.rankingElement.link];
    [self.cloudWebView loadRequest:[[NSURLRequest alloc] initWithURL:url]];
    [self.view addSubview:self.cloudWebView];
}

@end
