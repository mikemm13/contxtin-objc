//
//  CloudRankingTableViewCell.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialCloudRankingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *dateOfPublish;
@property (weak, nonatomic) IBOutlet UILabel *comment;

@end
