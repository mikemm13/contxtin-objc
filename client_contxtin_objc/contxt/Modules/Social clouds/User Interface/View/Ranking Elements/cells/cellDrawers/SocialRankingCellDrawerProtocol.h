//
//  RankingCellDrawerProtocol.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SocialRankingItemProtocol;
@protocol SocialRankingCellDrawerProtocol <NSObject>
- (UITableViewCell *)cellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
- (void)drawCell:(UITableViewCell *)cell withItem:(id<SocialRankingItemProtocol>)item;
@end
