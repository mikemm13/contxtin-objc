//
//  CloudRankingCellDrawer.m
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "SocialCloudRankingCellDrawer.h"
#import "Comment.h"
#import "SocialCloudRankingTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>


@implementation SocialCloudRankingCellDrawer
static BOOL categoryCellNibRegistered = NO;
- (instancetype)init
{
    self = [super init];
    if (self) {
        categoryCellNibRegistered = NO;
    }
    return self;
}
- (void)registerCellNibInTableViewIfNeeded:(UITableView *)tableView{
    if (!categoryCellNibRegistered) {
        NSString *cellClass = NSStringFromClass([SocialCloudRankingTableViewCell class]);
        [tableView registerNib:[UINib nibWithNibName:cellClass bundle:nil] forCellReuseIdentifier:cellClass];
        categoryCellNibRegistered = YES;
    }
}
- (UITableViewCell *)cellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath{
    [self registerCellNibInTableViewIfNeeded:tableView];
    return [tableView dequeueReusableCellWithIdentifier:@"SocialCloudRankingTableViewCell" forIndexPath:indexPath];
}
- (void)drawCell:(SocialCloudRankingTableViewCell *)cell withItem:(Comment *)item{
    cell.name.text = item.name;
    cell.comment.text = item.message;
//    [cell.comment sizeToFit];
}

- (UIImage *)messageBubbleImage {
    return [UIImage imageNamed:@"cloudIcon"];
}

@end
