//
//  ModelController.m
//  testPageVC
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "SocialCloudPageViewControllerDatasource.h"

#import "SocialCloudDataViewController.h"
#import "ContxtCategory.h"
#import "SocialCloudsWireFrame.h"


@interface SocialCloudPageViewControllerDatasource()
@property (strong, nonatomic) NSArray *clouds;

@end

@implementation SocialCloudPageViewControllerDatasource

- (id)init
{
    self = [super init];
    if (self) {
        
        
    }
    return self;
}

- (void)addObserverIfNeeded {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDataViewControllerWhenCloudsLoaded) name:@"cloudsUpdated" object:nil];
}

- (NSArray *)clouds{
    ContxtCategory *category = [ContxtCategory MR_findFirstByAttribute:@"category" withValue:self.categoryName inContext:[NSManagedObjectContext MR_defaultContext]];
    NSArray *cloudsList = [category.clouds allObjects];
    return cloudsList;
}

- (SocialCloudDataViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.clouds count] == 0) || (index >= [self.clouds count])) {
        return nil;
    }
    
    SocialCloudDataViewController *dataViewController = [[SocialCloudDataViewController alloc] initWithNibName:@"SocialCloudDataViewController" bundle:nil];
    SocialCloudsWireFrame *router = [[SocialCloudsWireFrame alloc] initWithCloudDataViewController:dataViewController];
    
    dataViewController.index = index;
    dataViewController.numberOfCloudsInCategory = self.clouds.count;
    dataViewController.sessionManager = self.sessionManager;
    dataViewController.dataObject = self.clouds[index];
    dataViewController.router = router;
    [dataViewController loadRankingElements];
    
    return dataViewController;
}

- (void) updateDataViewControllerWhenCloudsLoaded {
    [self.delegate updateViewControllers];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSUInteger)indexOfViewController:(SocialCloudDataViewController *)viewController
{   
    
    return [self.clouds indexOfObject:viewController.dataObject];
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(SocialCloudDataViewController *)viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(SocialCloudDataViewController *)viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.clouds count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

@end
