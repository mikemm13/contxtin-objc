//
//  CommentModelData.m
//  contxt
//
//  Created by Miguel Martin Nieto on 5/10/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "CommentModelData.h"
#import "Dialogue.h"
#import "Comment.h"


@implementation CommentModelData

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.messages = [NSMutableArray array];
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
        
        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleBlueColor]];
    }
    
    return self;
}

- (void)loadMessages {
    NSArray *messages;
    messages = [Comment MR_findByAttribute:@"dialogue" withValue:self.cloudPresented.dialogue andOrderBy:@"date" ascending:YES inContext:[NSManagedObjectContext MR_defaultContext]];
    NSMutableArray *messagesFormatted = [NSMutableArray array];
    for (Comment *comment in messages) {
        [messagesFormatted addObject:[[JSQMessage alloc] initWithSenderId:comment.uid
                                                       senderDisplayName:comment.name
                                                                    date:comment.date
                                                                     text:comment.message]];
    }
    self.messages = messagesFormatted;
}

@end
