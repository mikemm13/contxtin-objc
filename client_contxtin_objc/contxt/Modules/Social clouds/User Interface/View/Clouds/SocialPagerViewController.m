//
//  SocialPagerViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 4/10/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "SocialPagerViewController.h"
#import "SocialRankingTableViewController.h"
#import "SocialRankingElementsTableViewManager.h"
#import "CommentsViewController.h"

@interface SocialPagerViewController ()

@property (strong, nonatomic) SocialRankingTableViewController *socialRankingTableViewController;
@property (strong, nonatomic) CommentsViewController *commentsViewController;
@property (strong, nonatomic) SocialRankingElementsTableViewManager *tableViewManager;

@end

@implementation SocialPagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = self;
    self.delegate = self;
    [self reloadData];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
}

- (SocialRankingTableViewController *)socialRankingTableViewController {
    if (!_socialRankingTableViewController) {
        _socialRankingTableViewController = [[SocialRankingTableViewController alloc] initWithNibName:nil bundle:nil];
        _socialRankingTableViewController.sessionManager = self.sessionManager;
        _socialRankingTableViewController.cloudPresented = self.cloudPresented;
    }
    return _socialRankingTableViewController;
}

- (CommentsViewController *)commentsViewController {
    if (!_commentsViewController) {
        _commentsViewController = [[CommentsViewController alloc] init];
        _commentsViewController.sessionManager = self.sessionManager;
        _commentsViewController.cloudPresented = self.cloudPresented;
//        self.tableViewManager.tableView = _commentsTableViewController.tableView;
        
    }
    return _commentsViewController;
}

- (SocialRankingElementsTableViewManager *)tableViewManager{
    if (!_tableViewManager) {
        _tableViewManager = [[SocialRankingElementsTableViewManager alloc] init];
        _tableViewManager.sessionManager = self.sessionManager;
        _tableViewManager.cloudPresented = self.cloudPresented;
    }
    return _tableViewManager;
}

#pragma mark - ViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return 2;
}

#pragma mark - ViewPagerDataSource
- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    
    UILabel *label = [UILabel new];
    if (index == 0) {
        label.text = [NSString stringWithFormat:@"Comments"];
    } else {
        label.text = [NSString stringWithFormat:@"News"];
    }
    [label sizeToFit];
    
    return label;
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index{
    if (index == 0) {
        return self.commentsViewController;
    } else {
        
        return self.socialRankingTableViewController;
    }
}


- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    
    switch (component) {
        case ViewPagerIndicator:
            return [UIColor colorWithRed:0.0/255.0 green:128.0/255.0 blue:255.0/255.0 alpha:1];
        default:
            return color;
    }
}

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    if (option == ViewPagerOptionTabWidth) {
        return [[UIScreen mainScreen] bounds].size.width/2;
    }
    return value;
}


@end
