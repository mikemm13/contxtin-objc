//
//  CloudDataViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "SocialCloudDataViewController.h"
#import "Cloud.h"
#import "Ranking.h"
#import "SocialRankingElementsTableViewManager.h"
#import "SocialCloudRankingTableViewCell.h"
#import "Tag.h"
#import "SocialPagerViewController.h"

@interface SocialCloudDataViewController ()

@property (weak, nonatomic) IBOutlet UILabel *cloudIndex;
@property (weak, nonatomic) IBOutlet UILabel *totalClouds;
@property (weak, nonatomic) IBOutlet UITableView *rankingElementsTableView;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *tagLabels;
@property (strong, nonatomic) SocialRankingElementsTableViewManager *tableViewManager;
@property (weak, nonatomic) IBOutlet UIView *tabView;
@property (strong, nonatomic) SocialPagerViewController *socialPagerVC;

@end

@implementation SocialCloudDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    Cloud *cloud = self.dataObject;
    self.socialPagerVC.cloudPresented = cloud;
    [self.tabView addSubview:self.socialPagerVC.view];
    self.tabView.translatesAutoresizingMaskIntoConstraints = NO;
    self.socialPagerVC.view.translatesAutoresizingMaskIntoConstraints = NO;
    UIView *socialPagerView = self.socialPagerVC.view;
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(socialPagerView);
    [self.tabView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[socialPagerView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:viewsDictionary]];
    [self.tabView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[socialPagerView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:viewsDictionary]];
    self.rankingElementsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.cloudIndex.text =  [NSString stringWithFormat:@"%li", self.index+1];
    self.totalClouds.text = [NSString stringWithFormat:@"%li", (long)self.numberOfCloudsInCategory];
    
    
    NSArray *tags = [cloud.tags allObjects];
    NSUInteger index = 0;
    for (UILabel *tagLabel in self.tagLabels) {
        
        if (index < tags.count) {
            tagLabel.text = [(Tag *)[tags objectAtIndex:index] name];
        } else {
            tagLabel.text = @"";
        }
        index ++;
    }
}

-(SocialPagerViewController *)socialPagerVC {
    if (!_socialPagerVC) {
        _socialPagerVC = [[SocialPagerViewController alloc] init];
        _socialPagerVC.sessionManager = self.sessionManager;
        
    }
    return _socialPagerVC;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)loadRankingElements{
    
    self.tableViewManager.router = self.router;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
