//
//  CloudsRootViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "SocialCloudsRootViewController.h"
#import "SocialCloudsPageViewController.h"
#import "EmptyViewController.h"

@interface SocialCloudsRootViewController ()
@property (strong, nonatomic) SessionManager *sessionManager;
@property (copy, nonatomic) NSString *categoryName;
@property (weak, nonatomic) IBOutlet UIToolbar *actionsToolbar;
@property (weak, nonatomic) IBOutlet UIView *pageViewControllerContainer;
@end

@implementation SocialCloudsRootViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil categoryName:(NSString *)categoryName sessionManager:(SessionManager *)sessionManager{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        _categoryName = categoryName;
        _sessionManager = sessionManager;
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = self.categoryName;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCloudsLoaded) name:@"cloudsUpdated" object:nil];
    [self loadClouds];
}

- (void)loadClouds
{
    SocialCloudsPageViewController *cloudsPageViewController = [[SocialCloudsPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    cloudsPageViewController.categoryName = self.categoryName;
    cloudsPageViewController.sessionManager = self.sessionManager;
    cloudsPageViewController.view.frame = self.pageViewControllerContainer.bounds;
    [self.pageViewControllerContainer addSubview:cloudsPageViewController.view];
    [self addChildViewController:cloudsPageViewController];
    [self hideActionToolbarIfNeeded:cloudsPageViewController];
}

- (void)updateCloudsLoaded {
    for (UIView *view in self.pageViewControllerContainer.subviews) {
        [view removeFromSuperview];
    }
    for (UIViewController *viewController in self.childViewControllers) {
        [viewController removeFromParentViewController];
    }
    [self loadClouds];
}

- (void)hideActionToolbarIfNeeded:(SocialCloudsPageViewController *)cloudsPageViewController
{
    if (cloudsPageViewController.viewControllers.count == 1 && [[cloudsPageViewController.viewControllers lastObject] isKindOfClass:[EmptyViewController class]]) {
        self.actionsToolbar.hidden = YES;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
