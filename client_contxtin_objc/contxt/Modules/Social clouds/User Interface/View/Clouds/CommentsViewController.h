//
//  CommentsViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 5/10/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "JSQMessagesViewController.h"
#import "SessionManager.h"
#import "Cloud.h"
#import "CommentModelData.h"

@interface CommentsViewController : JSQMessagesViewController<UIActionSheetDelegate, JSQMessagesComposerTextViewPasteDelegate>

@property (strong, nonatomic) Cloud *cloudPresented;

@property (strong, nonatomic) SessionManager *sessionManager;

@property (strong, nonatomic) CommentModelData *commentData;

- (void)receiveMessagePressed:(UIBarButtonItem *)sender;

- (void)closePressed:(UIBarButtonItem *)sender;


@end
