//
//  ModelController.h
//  testPageVC
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"

@class SocialCloudDataViewController;

@protocol SocialCloudPageViewControllerDatasourceDelegate <NSObject>

- (void) updateViewControllers;

@end

@interface SocialCloudPageViewControllerDatasource : NSObject <UIPageViewControllerDataSource>

@property (copy, nonatomic) NSString *categoryName;
@property (strong, nonatomic) SessionManager *sessionManager;
@property (weak, nonatomic) id<SocialCloudPageViewControllerDatasourceDelegate> delegate;

- (SocialCloudDataViewController *)viewControllerAtIndex:(NSUInteger)index;
- (NSUInteger)indexOfViewController:(SocialCloudDataViewController *)viewController;
- (void)addObserverIfNeeded;

@end
