//
//  CommentModelData.h
//  contxt
//
//  Created by Miguel Martin Nieto on 5/10/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSQMessages.h"
#import "Cloud.h"

@interface CommentModelData : NSObject

@property (strong, nonatomic) Cloud *cloudPresented;

@property (strong, nonatomic) NSMutableArray *messages;

@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;

@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

- (void)loadMessages;

@end
