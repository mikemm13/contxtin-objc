//
//  CloudsRootViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"

@interface SocialCloudsRootViewController : UIViewController
- (instancetype)initWithNibName:(NSString *)nibNameOrNil categoryName:(NSString *)categoryName sessionManager:(SessionManager *)sessionManager;
@end
