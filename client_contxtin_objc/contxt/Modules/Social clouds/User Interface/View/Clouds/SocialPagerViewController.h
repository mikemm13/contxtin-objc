//
//  SocialPagerViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 4/10/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "ViewPagerController.h"
#import "Cloud.h"
#import "SessionManager.h"


@interface SocialPagerViewController : ViewPagerController<ViewPagerDataSource, ViewPagerDelegate>

@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) Cloud *cloudPresented;

@end
