//
//  CloudDataViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"

@protocol SocialCloudDataViewControllerRouter <NSObject>

- (void)loadSelectedItemContent:(UIViewController *)contentsViewController;

@end

@interface SocialCloudDataViewController : UIViewController
@property (strong, nonatomic) id dataObject;
@property (nonatomic) NSInteger index;
@property (nonatomic) NSInteger numberOfCloudsInCategory;
@property (strong, nonatomic) SessionManager *sessionManager;
@property (weak, nonatomic)id<SocialCloudDataViewControllerRouter> router;
- (void)loadRankingElements;
@end
