//
//  CloudsPageViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "SocialCloudsPageViewController.h"
#import "SocialCloudDataViewController.h"
#import "SocialCloudsWireFrame.h"
#import "EmptyViewController.h"

@interface SocialCloudsPageViewController ()
@property (strong, nonatomic) SocialCloudPageViewControllerDatasource *dataSourceController;
@property (strong, nonatomic) SocialCloudDataViewController *startingViewController;

@end

@implementation SocialCloudsPageViewController


- (instancetype)initWithNibName:(NSString *)nibNameOrNil categoryName:(NSString *)categoryName sessionManager:(SessionManager *)sessionManager{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        _categoryName = categoryName;
        _sessionManager = sessionManager;
        
        
        
    }
    return self;
}


- (void)viewDidLoad{
    [super viewDidLoad];
    [self loadViewControllers];

}

- (void)loadViewControllers {
    // Do any additional setup after loading the view from its nib.
    NSArray *viewControllers;
    if (self.startingViewController) {
        viewControllers = @[self.startingViewController];
        
        self.dataSource = self.dataSourceController;
    } else {
        EmptyViewController *emptyViewController = [[EmptyViewController alloc] initWithMessage:[self getEmptyMessage]];
        viewControllers = @[emptyViewController];
    }
    [self setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

- (SocialCloudDataViewController *)startingViewController{
    if (!_startingViewController) {
        _startingViewController = [self.dataSourceController viewControllerAtIndex:0 ];
    }
    return _startingViewController;
}

- (SocialCloudPageViewControllerDatasource *)dataSourceController{
    if (!_dataSourceController) {
        _dataSourceController = [[SocialCloudPageViewControllerDatasource alloc] init];
        _dataSourceController.sessionManager = self.sessionManager;
        _dataSourceController.categoryName = self.categoryName;
        _dataSourceController.delegate = self;
        [_dataSourceController addObserverIfNeeded];
    }
    return _dataSourceController;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getEmptyMessage {
    return @"There are no conversations yet";
}

- (void)updateViewControllers {
    [self loadViewControllers];
}

@end
