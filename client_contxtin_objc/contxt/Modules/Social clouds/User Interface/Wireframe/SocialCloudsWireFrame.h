//
//  CloudsWireFrame.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocialCloudDataViewController.h"

@interface SocialCloudsWireFrame : NSObject<SocialCloudDataViewControllerRouter>
@property (strong, nonatomic)SessionManager *sessionManager;
- (instancetype)initWithCloudDataViewController:(SocialCloudDataViewController *)cloudData;
- (void)loadSelectedItemContent:(UIViewController *)contentsViewController;

@end
