//
//  CloudsWireFrame.m
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "SocialCloudsWireFrame.h"

@interface SocialCloudsWireFrame()

@property (strong, nonatomic)SocialCloudDataViewController *cloudData;
@end

@implementation SocialCloudsWireFrame

- (instancetype)initWithCloudDataViewController:(SocialCloudDataViewController *)cloudData
{
    self = [super init];
    if (self) {
        _cloudData = cloudData;
        _cloudData.router = self;
    }
    return self;
}

- (void)loadSelectedItemContent:(UIViewController *)contentsViewController; {
    
    [self.cloudData.navigationController pushViewController:contentsViewController animated:YES];
}


@end
