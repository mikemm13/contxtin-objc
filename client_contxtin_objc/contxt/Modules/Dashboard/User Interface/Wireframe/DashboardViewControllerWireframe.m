//
//  DashboardViewControllerWireframe.m
//  contxt
//
//  Created by Miguel Martin Nieto on 20/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "DashboardViewControllerWireframe.h"
#import "LoginViewController.h"
#import "LoginWireFrame.h"
#import "InitialScreenViewController.h"

@interface DashboardViewControllerWireframe()
@property (weak, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic)DashboardViewController *dashboardViewController;
@property (strong, nonatomic) LoginWireFrame *loginWireFrame;
@property (strong, nonatomic) SignUpWireFrame *signUpWireFrame;
@end

@implementation DashboardViewControllerWireframe
- (instancetype)initWithNavigationController:(UINavigationController *)navigationController
{
    self = [super init];
    if (self) {
        _navigationController = navigationController;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loggedUserNotExists) name:@"userLoggedOut" object:nil];
    }
    return self;
}
- (DashboardViewController *)dashboardViewController{
    if (!_dashboardViewController) {
        _dashboardViewController = [[DashboardViewController alloc] initWithNibName:@"DashboardViewController" andSessionManager:self.sessionManager];
        _dashboardViewController.router = self;
    }
    return _dashboardViewController;
}

- (LoginWireFrame *)loginWireFrame {
    if (!_loginWireFrame) {
        _loginWireFrame = [[LoginWireFrame alloc] initWithSessionManager:self.sessionManager];
    }
    return _loginWireFrame;
}

- (SignUpWireFrame *)signUpWireFrame {
    if (!_signUpWireFrame) {
        _signUpWireFrame = [[SignUpWireFrame alloc] initWithSessionManager:self.sessionManager];
    }
    return _signUpWireFrame;
}

- (UIViewController *)rootViewController{
    return self.dashboardViewController;
}
- (void)checkLoggedUser{
    if (![self.sessionManager userLogged]) {
        [self loggedUserNotExists];
    }
}
-(void)loggedUserNotExists{

//    [self.navigationController presentViewController:self.loginWireFrame.rootViewController animated:NO completion:nil];
    UIStoryboard *authenticationStoryboard = [UIStoryboard storyboardWithName:@"Authentication" bundle:nil];
    UINavigationController *initialNavigationViewController = (UINavigationController *)[authenticationStoryboard instantiateViewControllerWithIdentifier:@"initialVC"];
    InitialScreenViewController *initialViewController = (InitialScreenViewController *)initialNavigationViewController.topViewController;
    initialViewController.sessionManager = self.sessionManager;
    initialViewController.loginWireFrame = self.loginWireFrame;
    initialViewController.signUpWireFrame = self.signUpWireFrame;
    [self.navigationController presentViewController:initialNavigationViewController animated:NO completion:nil];

}

- (void)loadSelectedItemContent:(UIViewController *)contentsViewController {

    [self.navigationController pushViewController:contentsViewController animated:YES];
}

@end
