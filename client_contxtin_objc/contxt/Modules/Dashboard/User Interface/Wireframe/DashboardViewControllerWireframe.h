//
//  DashboardViewControllerWireframe.h
//  contxt
//
//  Created by Miguel Martin Nieto on 20/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DashboardViewController.h"
#import "SessionManager.h"

@interface DashboardViewControllerWireframe : NSObject<DashboardViewControllerRouter>
@property (nonatomic , readonly) UIViewController *rootViewController;
@property (strong, nonatomic)SessionManager *sessionManager;
- (instancetype)initWithNavigationController:(UINavigationController *)navigationController;
- (void)loadSelectedItemContent:(UIViewController *)contentsViewController;
- (void)checkLoggedUser;
@end
