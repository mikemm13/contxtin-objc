//
//  DashboardTableViewManager.h
//  contxt
//
//  Created by Miguel Martin Nieto on 21/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"
#import "DashboardViewControllerWireframe.h"

@interface DashboardTableViewManager : NSObject<NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) DashboardViewControllerWireframe *router;
- (void)loadFetchedResultController;
- (void)unregisterCell;
@end
