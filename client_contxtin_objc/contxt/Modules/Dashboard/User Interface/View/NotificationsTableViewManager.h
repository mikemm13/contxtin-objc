//
//  NotificationsTableViewManager.h
//  contxt
//
//  Created by Miguel Martin Nieto on 26/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"
#import "NotificationsViewController.h"

@interface NotificationsTableViewManager : NSObject<NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) SessionManager *sessionManager;
@property (weak, nonatomic) NotificationsViewController *notificationsViewController;
- (void)unregisterCell;


@end
