//
//  DashboardViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"

@protocol DashboardViewControllerRouter <NSObject>

-(void)loggedUserNotExists;
- (void)loadSelectedItemContent:(UIViewController *)contentsViewController;

@end

@interface DashboardViewController : UITableViewController

@property (weak, nonatomic)id<DashboardViewControllerRouter> router;

- (instancetype)initWithNibName:(NSString *)nibName andSessionManager:(SessionManager *)sessionManager;
- (void)loadInitialView;

@end
