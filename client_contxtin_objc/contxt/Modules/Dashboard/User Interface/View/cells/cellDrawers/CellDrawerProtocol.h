//
//  CellDrawerProtocol.h
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DashboardItemProtocol;
@protocol CellDrawerProtocol <NSObject>
- (UITableViewCell *)cellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
- (void)drawCell:(UITableViewCell *)cell withItem:(id<DashboardItemProtocol>)item;
- (void)unregisterCellNib;
@end
