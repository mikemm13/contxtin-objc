//
//  CategoryCellDrawer.h
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CellDrawerProtocol.h"
#import "CategoryCellDrawerProtocol.h"

@interface DashboardCategoryCellDrawer : NSObject<CellDrawerProtocol, CategoryCellDrawerProtocol>

@end
