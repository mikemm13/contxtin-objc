//
//  CategoryCellDrawer.m
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "DashboardCategoryCellDrawer.h"
#import "ContxtCategory.h"
#import "DashboardCategoryTableViewCell.h"
#import "Cloud.h"
#import "Ranking.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation DashboardCategoryCellDrawer


static BOOL dashboardCategoryCellNibRegistered = NO;
- (void)registerCellNibInTableViewIfNeeded:(UITableView *)tableView{
    if (!dashboardCategoryCellNibRegistered) {
        NSString *cellClass = NSStringFromClass([DashboardCategoryTableViewCell class]);
        [tableView registerNib:[UINib nibWithNibName:cellClass bundle:nil] forCellReuseIdentifier:cellClass];
        dashboardCategoryCellNibRegistered = YES;
    }
}

- (void)unregisterCellNib {
    dashboardCategoryCellNibRegistered = NO;
}

- (UITableViewCell *)cellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath{
    [self registerCellNibInTableViewIfNeeded:tableView];
    return [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DashboardCategoryTableViewCell class]) forIndexPath:indexPath];
}
- (void)drawCell:(DashboardCategoryTableViewCell *)cell withItem:(ContxtCategory *)item{
    cell.categoryName.text = item.category;
    cell.categoryImageView.image = [UIImage imageNamed:@"placeholderImage.jpg"];
    NSArray *clouds = [item.clouds allObjects];
    if (clouds.count) {
        Cloud *cloud = [clouds objectAtIndex:0];
        NSArray *rankings = [cloud.rankingElements allObjects];
        if (rankings.count) {
            Ranking *ranking = [rankings objectAtIndex:0];
            if (![ranking.image isEqualToString:@""]) {
                NSURL *url = [NSURL URLWithString:ranking.image];
                
                [cell.categoryImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholderImage.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error) {
                    }
                }];
            } else {
                cell.categoryImageView.image = [UIImage imageNamed:@"placeholderImage.jpg"];
            }
            
        }
    }
    
}

@end
