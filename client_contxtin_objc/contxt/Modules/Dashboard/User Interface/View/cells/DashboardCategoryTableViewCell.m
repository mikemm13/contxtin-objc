//
//  CategoryTableViewCell.m
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "DashboardCategoryTableViewCell.h"

@implementation DashboardCategoryTableViewCell

- (void)awakeFromNib
{
    // Initialization code
//    self.categoryImageView = [[UIImageView alloc] initWithFrame:self.frame];
//    self.categoryImageView.contentMode = UIViewContentModeScaleAspectFill;
//    [self.contentView addSubview:self.categoryImageView];
//    self.clipsToBounds = YES;
    self.categoryImageView.frame = self.frame;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews{
//    [super layoutSubviews];
//    self.categoryImageView.frame = self.frame;
}

@end
