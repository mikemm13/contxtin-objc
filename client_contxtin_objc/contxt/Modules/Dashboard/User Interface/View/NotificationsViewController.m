//
//  NotificationsViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 26/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "NotificationsViewController.h"
#import "NotificationsTableViewManager.h"
#import "NotificationsInteractor.h"
#import "Notification.h"

@interface NotificationsViewController ()

@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) NotificationsTableViewManager *tableViewManager;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;

@end

@implementation NotificationsViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil sessionManager:(id)sessionManager {
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        _sessionManager = sessionManager;
        _tableViewManager = [[NotificationsTableViewManager alloc] init];
        _tableViewManager.notificationsViewController = self;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Notifications";
    [self.clearButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [self loadInitialView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.tableViewManager unregisterCell];
    [super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.tableViewManager.sessionManager = self.sessionManager;
    self.tableViewManager.tableView = self.tableView;
}

- (void)loadInitialView{
    self.tableViewManager.sessionManager = self.sessionManager;
    self.tableViewManager.tableView = self.tableView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (IBAction)clearAllButtonPressed:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Clear all notifications" message:@"Do you want to clear all notifications?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NotificationsInteractor *notificationsInteractor = [[NotificationsInteractor alloc] initWithSessionManager:self.sessionManager];
        [notificationsInteractor clearAllNotifications:[Notification MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]]];
        NSPredicate *predicate = [NSPredicate predicateWithValue:YES];
        [Notification MR_deleteAllMatchingPredicate:predicate inContext:[NSManagedObjectContext MR_defaultContext]];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            
        }];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}


@end
