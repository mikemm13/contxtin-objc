//
//  NotificationsViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 26/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"

@interface NotificationsViewController : UIViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil sessionManager:(SessionManager *)sessionManager;

@end
