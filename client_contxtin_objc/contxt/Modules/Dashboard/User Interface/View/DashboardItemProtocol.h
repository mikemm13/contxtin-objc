//
//  DashboardItemProtocol.h
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CellDrawerProtocol.h"
#import "CategoryCellDrawerProtocol.h"
#import "SessionManager.h"

@protocol DashboardItemProtocol <NSObject>
@property (copy,nonatomic,readonly) NSString *itemTitle;
@property (strong,nonatomic,readonly) id<CellDrawerProtocol, CategoryCellDrawerProtocol> cellDrawer;
- (UIViewController *)contentsViewControllerWithSessionManager:(SessionManager *)sessionManager;
@end
