//
//  DashboardViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "DashboardViewController.h"
#import "DashboardInteractor.h"
#import "DashboardItemProtocol.h"
#import "DashboardTableViewManager.h"
#import "UIViewController+AMSlideMenu.h"
#import "Notification.h"
#import "NotificationsViewController.h"

@interface DashboardViewController ()
@property (strong, nonatomic) DashboardInteractor *dashboardInteractor;
@property (strong, nonatomic) NSArray *items;
@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) DashboardTableViewManager *tableViewManager;
@property (strong, nonatomic) UIView *titleImageView;
@property (strong, nonatomic) UIBarButtonItem *rightBarButton;
@end

@implementation DashboardViewController

- (instancetype)initWithNibName:(NSString *)nibName andSessionManager:(SessionManager *)sessionManager
{
    self = [super initWithNibName:nibName bundle:nil];
    if (self) {
        _sessionManager = sessionManager;
        _tableViewManager = [[DashboardTableViewManager alloc] init];
        _titleImageView = [self createTitleImageView];
    }
    return self;
}

-(DashboardInteractor *)dashboardInteractor{
    if (!_dashboardInteractor) {
        _dashboardInteractor = [[DashboardInteractor alloc] initWithSessionManager:self.sessionManager];
    }
    return _dashboardInteractor;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addLeftMenuButton];
    [self addRightNavigationButton];
    [self loadInitialView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addRightNavigationButton) name:@"notificationsUpdated" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.tableViewManager unregisterCell];
    [super viewWillDisappear:animated];
}

- (UIBarButtonItem *)rightBarButton {
    if (!_rightBarButton) {
        _rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bellIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(notificationButtonPressed)];
    }
    return _rightBarButton;
}

- (UIView *)createTitleImageView {
    UIView *backView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];// Here you can set View width and height as per your requirement for displaying titleImageView position in navigationbar
    
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"contxt-logo"]];
    titleImageView.frame = CGRectMake(0, 0,40 , 40); // Here I am passing origin as (45,5) but can pass them as your requirement.
    [backView addSubview:titleImageView];
    return backView;
}

- (void)addRightNavigationButton {
    NSArray *notifications = [Notification MR_findByAttribute:@"user" withValue:self.sessionManager.userLogged inContext:[NSManagedObjectContext MR_defaultContext]];
    if (notifications.count) {
        self.rightBarButton.enabled = YES;
    } else {
        self.rightBarButton.enabled = NO;
    }
    self.navigationItem.rightBarButtonItem = self.rightBarButton;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self addLeftMenuButton];
    [self addRightNavigationButton];
    [self loadInitialView];
}

- (void)loadInitialView{
    self.navigationItem.titleView = self.titleImageView;
    self.tableViewManager.router = self.router;
    self.tableViewManager.sessionManager = self.sessionManager;
    self.tableViewManager.tableView = self.tableView;
}

- (BOOL)checkIfUserIsLogged{
    return [self.dashboardInteractor userLogged];
}

- (void)notificationButtonPressed {
    NotificationsViewController *notificationsViewController = [[NotificationsViewController alloc] initWithNibName:@"NotificationsViewController" sessionManager:self.sessionManager];
    [self.router loadSelectedItemContent:notificationsViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
