//
//  DashboardTableViewManager.m
//  contxt
//
//  Created by Miguel Martin Nieto on 21/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "DashboardTableViewManager.h"
#import "DashboardItemProtocol.h"
#import "ContxtCategory.h"
#import "ContxtCategory+DashboardItem.h"

@interface DashboardTableViewManager ()

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic) BOOL screenHasBeenSet;

@end

NSInteger const dashboardItemsHeight = 150;

@implementation DashboardTableViewManager


- (void)setTableView:(UITableView *)tableView{
    _tableView = tableView;
    _tableView.rowHeight = dashboardItemsHeight;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView reloadData];
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"selected = %@",@1];
    _fetchedResultsController = [ContxtCategory MR_fetchAllSortedBy:@"category" ascending:YES withPredicate:predicate groupBy:nil delegate:self];
    
    return _fetchedResultsController;
}

- (void) setCategoryScreen {
    for (ContxtCategory *contxtCategory in _fetchedResultsController.fetchedObjects) {
        contxtCategory.screenWhereCategoryIsShown = @"Dashboard";
    }
    self.screenHasBeenSet = YES;
}

- (void)unregisterCell {
    if ([self.tableView numberOfRowsInSection:0] > 0) {
        NSIndexPath *indexPath = [[self.tableView indexPathsForVisibleRows] firstObject];
        id<DashboardItemProtocol> item = (id<DashboardItemProtocol>)[self.fetchedResultsController objectAtIndexPath:indexPath];
        [item.cellDrawer unregisterCellNib];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count=[[self.fetchedResultsController.sections objectAtIndex:0] numberOfObjects];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setCategoryScreen];

    id<DashboardItemProtocol> item = (id<DashboardItemProtocol>)[self.fetchedResultsController objectAtIndexPath:indexPath];
    UITableViewCell *cell = [item.cellDrawer cellForTableView:tableView atIndexPath:indexPath];
    [item.cellDrawer drawCell:cell withItem:item];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    id<DashboardItemProtocol> item = (id<DashboardItemProtocol>)[self.fetchedResultsController objectAtIndexPath:indexPath];
    [self.router loadSelectedItemContent:[item contentsViewControllerWithSessionManager:self.sessionManager]];
}


#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableViewRowAnimation animation = UITableViewRowAnimationFade;
    NSIndexPath *changeIndexPath=indexPath;
    NSIndexPath *newChangeIndexPath=newIndexPath;
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newChangeIndexPath] withRowAnimation:animation];
            break;
        case NSFetchedResultsChangeMove:
            if ([self.tableView respondsToSelector:@selector(moveRowAtIndexPath:toIndexPath:)]) {
                [self.tableView moveRowAtIndexPath:changeIndexPath toIndexPath:newChangeIndexPath];
            } else {
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:changeIndexPath] withRowAnimation:animation];
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:newChangeIndexPath.section] withRowAnimation:animation];
            }
            break;
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:changeIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:changeIndexPath] withRowAnimation:animation];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    NSIndexSet *indexSetWithSectionIndex = [NSIndexSet indexSetWithIndex:sectionIndex];
    
    UITableViewRowAnimation animation = UITableViewRowAnimationFade;
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:indexSetWithSectionIndex withRowAnimation:animation];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:indexSetWithSectionIndex withRowAnimation:animation];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.tableView endUpdates];
}


@end
