//
//  DashboardInteractor.h
//  contxt
//
//  Created by Miguel Martin Nieto on 19/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"

@interface DashboardInteractor : NSObject

- (instancetype)initWithSessionManager:(SessionManager *)sessionManager;
- (BOOL)userLogged;
@end
