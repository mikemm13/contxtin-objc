//
//  DashboardInteractor.m
//  contxt
//
//  Created by Miguel Martin Nieto on 19/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "DashboardInteractor.h"

@interface DashboardInteractor()

@property (strong, nonatomic)SessionManager *sessionManager;

@end

@implementation DashboardInteractor

- (instancetype)initWithSessionManager:(SessionManager *)sessionManager
{
    self = [super init];
    if (self) {
        _sessionManager = sessionManager;
    }
    return self;
}

-(BOOL)userLogged{
    if ([self.sessionManager userLogged]) {
        return YES;
    }
    return NO;
}

@end
