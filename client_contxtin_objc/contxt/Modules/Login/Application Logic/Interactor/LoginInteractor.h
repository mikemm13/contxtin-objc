//
//  LoginInteractor.h
//  contxt
//
//  Created by Miguel Martin Nieto on 17/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"

@interface LoginInteractor : NSObject

-(instancetype)initWithSessionManager:(SessionManager *)sessionManager;
-(void)loginWithEmail:(NSString *)email andPassword:(NSString *)password completion:(void (^)(BOOL loginSucceeded, NSString *message))completion ;
- (void)logoutWithCompletion:(void (^)(BOOL logoutSucceeded, NSString *message))completion;
@end
