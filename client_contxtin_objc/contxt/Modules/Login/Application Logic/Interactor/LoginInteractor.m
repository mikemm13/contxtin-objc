//
//  LoginInteractor.m
//  contxt
//
//  Created by Miguel Martin Nieto on 17/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "LoginInteractor.h"

@interface LoginInteractor()
@property (strong,nonatomic) SessionManager *sessionManager;

@end

@implementation LoginInteractor

- (instancetype)initWithSessionManager:(SessionManager *)sessionManager
{
    self = [super init];
    if (self) {
        _sessionManager = sessionManager;
    }
    return self;
}

- (void)loginWithEmail:(NSString *)email andPassword:(NSString *)password completion:(void (^)(BOOL loginSucceeded, NSString *message))completion{
    
    [self.sessionManager performLoginWithEmail:email andPassword:password success:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedSuccessfully" object:self];
        completion(YES, @"");
    } failure:^(NSString *message) {
        completion(NO, message);
    }];
}

- (void)logoutWithCompletion:(void (^)(BOOL logoutSucceeded, NSString *message))completion{
    [self.sessionManager performLogoutWithSuccessBlock:^{
        completion(YES, @"");
    } failure:^(NSString *message) {
        completion(NO, message);
    }];
}

@end
