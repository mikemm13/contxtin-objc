//
//  SignUpInteractor.m
//  contxt
//
//  Created by Miguel Martin Nieto on 28/5/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "SignUpInteractor.h"

@interface SignUpInteractor ()

@property (strong, nonatomic) SessionManager *sessionManager;

@end

@implementation SignUpInteractor

- (instancetype)initWithSessionManager:(SessionManager *)sessionManager
{
    self = [super init];
    if (self) {
        _sessionManager = sessionManager;
    }
    return self;
}

- (void)signUpWithUserName:(NSString *)username email:(NSString *)email andPassword:(NSString *)password completion:(void (^)(BOOL, NSString *))completion {
    [self.sessionManager performSignUpWithUsername:username email:email andPassword:password success:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userSignedUpSuccessfully" object:self];
        completion(YES, @"");
    } failure:^(NSString *message) {
        completion(NO, message);
    }];
}

@end
