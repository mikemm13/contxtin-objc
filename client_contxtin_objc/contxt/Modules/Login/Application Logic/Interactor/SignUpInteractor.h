//
//  SignUpInteractor.h
//  contxt
//
//  Created by Miguel Martin Nieto on 28/5/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"

@interface SignUpInteractor : NSObject

-(instancetype)initWithSessionManager:(SessionManager *)sessionManager;

-(void)signUpWithUserName:(NSString *)username email:(NSString *)email andPassword:(NSString *)password completion:(void (^)(BOOL signUpSucceeded, NSString *message))completion ;

@end
