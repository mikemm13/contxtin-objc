//
//  LoginWireFrame.m
//  contxt
//
//  Created by Miguel Martin Nieto on 17/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "LoginWireFrame.h"
#import "MenuMainViewController.h"
#import "AppDelegate.h"

@interface LoginWireFrame ()
@property (strong, nonatomic) LoginViewController *loginViewController;
@end

@implementation LoginWireFrame

- (instancetype)initWithSessionManager:(SessionManager *)sessionManager;
{
    self = [super init];
    if (self) {
        _sessionManager = sessionManager;
        
    }
    return self;
}

- (UIViewController *)rootViewController{
    return self.loginViewController;
}

-(void)userLoggedSuccessfully:(UIViewController *)loginVC{
    [loginVC.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


@end
