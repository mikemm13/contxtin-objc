//
//  SignUpWireFrame.h
//  contxt
//
//  Created by Miguel Martin Nieto on 28/5/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SignUpViewController.h"
#import "SessionManager.h"

@interface SignUpWireFrame : NSObject<SignUpViewControllerRouter>

@property (strong, nonatomic)SessionManager *sessionManager;
@property (nonatomic , readonly) UIViewController *rootViewController;
- (instancetype)initWithSessionManager:(SessionManager *)sessionManager;
- (void)userSignedUpSuccessfully:(UIViewController *)signUpVC;

@end
