//
//  SignUpWireFrame.m
//  contxt
//
//  Created by Miguel Martin Nieto on 28/5/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "SignUpWireFrame.h"
#import "CategoriesTableViewController.h"

@interface SignUpWireFrame ()

@property (strong, nonatomic) SignUpViewController *signUpViewController;

@end

@implementation SignUpWireFrame

- (instancetype)initWithSessionManager:(SessionManager *)sessionManager;
{
    self = [super init];
    if (self) {
        _sessionManager = sessionManager;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadCategories) name:@"categoriesLoaded" object:nil];
    }
    return self;
}

- (UIViewController *)rootViewController{
    return self.signUpViewController;
}

-(void)userSignedUpSuccessfully:(SignUpViewController *)signUpVC {
    //show message
    self.signUpViewController = signUpVC;
}

- (void)loadCategories {
    CategoriesTableViewController *categoriesTableViewController = [[CategoriesTableViewController alloc] initWithNibName:@"CategoriesTableViewController" sessionManager:self.sessionManager];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:categoriesTableViewController];
    [self.signUpViewController presentViewController:navigationController animated:YES completion:^{
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }];
    
}




@end
