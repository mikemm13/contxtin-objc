//
//  LoginWireFrame.h
//  contxt
//
//  Created by Miguel Martin Nieto on 17/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"
#import "LoginViewController.h"

@interface LoginWireFrame : NSObject<LoginViewControllerRouter>

@property (strong, nonatomic)SessionManager *sessionManager;
@property (nonatomic , readonly) UIViewController *rootViewController;
- (instancetype)initWithSessionManager:(SessionManager *)sessionManager;
- (void)userLoggedSuccessfully:(UIViewController *)loginVC;

@end
