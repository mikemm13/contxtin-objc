//
//  SignUpViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 28/5/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "SignUpViewController.h"
#import "SignUpInteractor.h"
#import "SessionManager.h"

@interface SignUpViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailAccountTextField;
@property (weak, nonatomic) IBOutlet UITextField *userPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *verifiedPasswordTextField;

@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) SignUpInteractor *signUpInteractor;

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.usernameTextField.delegate = self;
    self.emailAccountTextField.delegate = self;
    self.userPasswordTextField.delegate = self;
    self.verifiedPasswordTextField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (SignUpInteractor *)signUpInteractor {
    if (!_signUpInteractor) {
        _signUpInteractor = [[SignUpInteractor alloc] initWithSessionManager:self.sessionManager];
    }
    return _signUpInteractor;
}

- (IBAction)onPressSignUp:(id)sender {
    @weakify(self)
    [self.signUpInteractor signUpWithUserName:self.usernameTextField.text email:self.emailAccountTextField.text andPassword:self.userPasswordTextField.text completion:^(BOOL signUpSucceeded, NSString *message) {
        @strongify(self)
        if (signUpSucceeded) {
            [self.router userSignedUpSuccessfully:self];
        } else {
            [self showError:message];
        }
    }];
}

- (void)showError:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sign up Error" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alertView show];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField isEqual:self.usernameTextField]) {
        [self.emailAccountTextField becomeFirstResponder];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if ([textField isEqual:self.emailAccountTextField]) {
        [self.userPasswordTextField becomeFirstResponder];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if ([textField isEqual:self.userPasswordTextField]){
        [self.verifiedPasswordTextField becomeFirstResponder];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if ([textField isEqual:self.verifiedPasswordTextField]) {
        [textField resignFirstResponder];
    }
    return YES;
}

@end
