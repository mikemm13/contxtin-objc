//
//  InitialScreenViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 24/5/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "InitialScreenViewController.h"
#import "SignUpViewController.h"

@interface InitialScreenViewController ()

@end

@implementation InitialScreenViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [segue.destinationViewController setValue:self.sessionManager forKey:@"sessionManager"];
    if ([segue.destinationViewController isKindOfClass:[SignUpViewController class]]) {
        [segue.destinationViewController setValue:self.signUpWireFrame forKey:@"router"];
    } else {
        [segue.destinationViewController setValue:self.loginWireFrame forKey:@"router"];
    }
}

@end
