//
//  LoginViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 20/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginInteractor.h"

@interface LoginViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (strong, nonatomic)SessionManager *sessionManager;
@property (strong, nonatomic)LoginInteractor *loginInteractor;

@end

@implementation LoginViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil sessionManager:(SessionManager *)sessionManager
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        _sessionManager = sessionManager;
        _loginInteractor = [[LoginInteractor alloc] initWithSessionManager:self.sessionManager];
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (LoginInteractor *)loginInteractor {
    if (!_loginInteractor) {
        _loginInteractor = [[LoginInteractor alloc] initWithSessionManager:self.sessionManager];
    }
    return _loginInteractor;
}

- (IBAction)pressedLoginButton:(id)sender {
    @weakify(self)
    [self.loginInteractor loginWithEmail:self.emailTextField.text andPassword:self.passwordTextField.text completion:^(BOOL loginSucceeded, NSString *message) {
        @strongify(self)
        if (loginSucceeded) {
            [self.router userLoggedSuccessfully:self];
        } else {
            [self showError:message];
        }
    }];
}




- (void)showError:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login Error" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alertView show];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField isEqual:self.emailTextField]){
        [self.passwordTextField becomeFirstResponder];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if ([textField isEqual:self.passwordTextField]) {
        [textField resignFirstResponder];
    }
    return YES;
}


@end
