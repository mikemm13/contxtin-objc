//
//  LoginViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 20/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"

@protocol LoginViewControllerRouter <NSObject>

-(void)userLoggedSuccessfully:(UIViewController *)viewController;

@end

@interface LoginViewController : UITableViewController
@property (weak, nonatomic) id<LoginViewControllerRouter> router;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil sessionManager:(SessionManager *)sessionManager;
@end
