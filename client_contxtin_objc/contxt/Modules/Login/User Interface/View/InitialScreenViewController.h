//
//  InitialScreenViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 24/5/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"
#import "LoginWireFrame.h"
#import "SignUpWireFrame.h"

@interface InitialScreenViewController : UIViewController

@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) LoginWireFrame *loginWireFrame;
@property (strong, nonatomic) SignUpWireFrame *signUpWireFrame;
@end
