//
//  SignUpViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 28/5/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SignUpViewControllerRouter <NSObject>

- (void) userSignedUpSuccessfully:(UIViewController *)viewController;

@end

@interface SignUpViewController : UITableViewController

@property (weak, nonatomic) id<SignUpViewControllerRouter> router;


@end
