//
//  MainViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 22/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"
#import "CategoriesAndCloudsUpdater.h"

@interface MainViewController : UINavigationController
@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) CategoriesAndCloudsUpdater *updater;
@end
