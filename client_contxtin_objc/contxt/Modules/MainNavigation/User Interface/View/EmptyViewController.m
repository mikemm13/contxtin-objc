//
//  EmptyViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 4/5/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "EmptyViewController.h"

@interface EmptyViewController ()

@property (nonatomic, copy) NSString *message;

@end

@implementation EmptyViewController

- (instancetype)initWithMessage:(NSString *)message
{
    self = [super initWithNibName:@"EmptyViewController" bundle:nil];
    if (self) {
        _message = message;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.messageLabel.text = self.message;
}

@end
