//
//  MenuMainViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 24/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "MenuMainViewController.h"
#import "LeftMenuViewController.h"

@interface MenuMainViewController ()

@end

@implementation MenuMainViewController




- (void)viewDidLoad
{
    
    [self loadMainScreen];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)configureLeftMenuButton:(UIButton *)button{
    CGRect frame = button.frame;
    frame.origin = (CGPoint){0,0};
    frame.size = (CGSize){40,40};
    button.frame = frame;
    
    [button setImage:[UIImage imageNamed:@"menuButton.jpeg"] forState:UIControlStateNormal];
}

- (void) loadMainScreen {
    self.leftMenu = nil;
    self.leftMenu = [[LeftMenuViewController alloc] initWithNibName:@"LeftMenuViewController" sessionManager:self.sessionManager ];
}

@end
