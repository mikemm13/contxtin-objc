//
//  MenuMainViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 24/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "AMSlideMenuMainViewController.h"
#import "SessionManager.h"

@interface MenuMainViewController : AMSlideMenuMainViewController
@property (strong, nonatomic) SessionManager *sessionManager;

-(void)loadMainScreen;

@end
