//
//  MainViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 22/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "MainViewController.h"
#import "DashboardViewControllerWireframe.h"

@interface MainViewController ()
@property (strong, nonatomic) DashboardViewControllerWireframe *dashboardWireFrame;
@end

@implementation MainViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
    }
    return self;
}

- (DashboardViewControllerWireframe *)dashboardWireFrame {
    if (!_dashboardWireFrame) {
        _dashboardWireFrame = [[DashboardViewControllerWireframe alloc] initWithNavigationController:self];
    }
    return _dashboardWireFrame;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationBar.translucent = NO;
    self.dashboardWireFrame.sessionManager = self.sessionManager;

    [self.updater loadCategoriesAndClouds];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.viewControllers = @[self.dashboardWireFrame.rootViewController];
    [self.dashboardWireFrame checkLoggedUser];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
