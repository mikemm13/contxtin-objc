//
//  LeftMenuViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 24/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "AMSlideMenuLeftTableViewController.h"
#import "SessionManager.h"


@interface LeftMenuViewController : AMSlideMenuLeftTableViewController
@property (strong, nonatomic) IBOutlet UITableView *view;
@property (strong, nonatomic) NSMutableArray *tableData;
- (id)initWithNibName:(NSString *)nibNameOrNil sessionManager:(SessionManager *)sessionManager;
@end
