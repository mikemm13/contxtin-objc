//
//  LeftMenuViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 24/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "DashboardViewController.h"
#import "CategoriesAndCloudsUpdater.h"
#import "MainViewController.h"
#import "SocialCloudsRootViewController.h"
#import "CategoriesTableViewController.h"
#import "LoginInteractor.h"
#import "LoginWireFrame.h"
#import "UIViewController+AMSlideMenu.h"
#import "CloudsRootViewController.h"
#import "FriendsViewController.h"

@interface LeftMenuViewController ()

@property (strong,nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) MainViewController *mainViewController;
@property (strong, nonatomic) SocialCloudsRootViewController *socialRootVC;
@property (strong, nonatomic) CategoriesTableViewController *categoriesVC;
@property (strong, nonatomic) CloudsRootViewController *savedRootVC;
@property (strong, nonatomic) FriendsViewController *friendsVC;

@end

@implementation LeftMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil sessionManager:(SessionManager *)sessionManager
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        // Custom initialization
        _sessionManager = sessionManager;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tableData = [@[@"My Clouds",@"Conversations", @"Saved", @"Categories", @"Contxt", @"Logout"] mutableCopy];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - TableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.textLabel.text = self.tableData[indexPath.row];
    return cell;
}

- (MainViewController *)mainViewController {
    if (!_mainViewController) {
        CategoriesAndCloudsUpdater *updater = [[CategoriesAndCloudsUpdater alloc] initWithSessionManager:self.sessionManager];
        _mainViewController = [[MainViewController alloc] init];
        _mainViewController.sessionManager = self.sessionManager;
        _mainViewController.updater = updater;
        
    }
    return _mainViewController;
}

- (SocialCloudsRootViewController *)socialRootVC {
    if (!_socialRootVC) {
        _socialRootVC = [[SocialCloudsRootViewController alloc] initWithNibName:@"SocialCloudsRootViewController" categoryName:@"Conversations" sessionManager:self.sessionManager];
    }
    return _socialRootVC;
}

- (CategoriesTableViewController *)categoriesVC {
    if (!_categoriesVC) {
        _categoriesVC = [[CategoriesTableViewController alloc] initWithNibName:@"CategoriesTableViewController" sessionManager:self.sessionManager];
    }
    return _categoriesVC;
}

- (CloudsRootViewController *)savedRootVC {
    if (!_savedRootVC) {
        _savedRootVC = [[CloudsRootViewController alloc] initWithNibName:@"CloudsRootViewController" categoryName:nil sessionManager:self.sessionManager isSavedScreen:YES];
    }
    return _savedRootVC;
}

- (FriendsViewController *)friendsVC {
    if (!_friendsVC) {
        _friendsVC = [[FriendsViewController alloc] initWithNibName:@"FriendsViewController" sessionManager:self.sessionManager];
    }
    return _friendsVC;
}

#pragma mark - TableView delegates


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        [self openContentNavigationController:self.mainViewController];
    } else if (indexPath.row == 1) {
        UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:self.socialRootVC];
        nvc.navigationBar.translucent = NO;
        [self openContentNavigationController:nvc];
    } else if (indexPath.row == 2) {
        UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:self.savedRootVC];
        [self openContentNavigationController:nvc];
        nvc.navigationBar.translucent = NO;
    } else if (indexPath.row == 3){
        UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:self.categoriesVC];
        nvc.navigationBar.translucent = NO;
        [self openContentNavigationController:nvc];
    } else if (indexPath.row == 4) {
        UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:self.friendsVC];
        nvc.navigationBar.translucent = NO;
        [self openContentNavigationController:nvc];
    } else if (indexPath.row == 5) {
        [self showLogoutAlertView];
    }
}

- (void)showLogoutAlertView {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Logout" message:@"Are you sure you want to proceed?" preferredStyle:UIAlertControllerStyleAlert];
    @weakify(self)
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        @strongify(self)
        LoginInteractor *loginInteractor = [[LoginInteractor alloc] initWithSessionManager:self.sessionManager];
        @weakify(self)
        [loginInteractor logoutWithCompletion:^(BOOL logoutSucceeded, NSString *message) {
            @strongify(self)
            [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedOut" object:nil];
            [self.mainVC closeLeftMenuAnimated:NO];
        }];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
@end
