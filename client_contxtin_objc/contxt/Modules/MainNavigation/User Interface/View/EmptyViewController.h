//
//  EmptyViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 4/5/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

- (instancetype) initWithMessage:(NSString *)message;

@end
