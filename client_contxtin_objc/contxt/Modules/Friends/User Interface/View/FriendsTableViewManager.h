//
//  FriendsTableViewManager.h
//  contxt
//
//  Created by Miguel Martin Nieto on 20/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"
#import "FriendsTableViewCell.h"
@class FriendsViewController;

@interface FriendsTableViewManager : NSObject<NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate, FriendsTableViewCellDelegate>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) SessionManager *sessionManager;
@property (weak, nonatomic) FriendsViewController *friendsViewController;
- (void)loadFetchedResultController;
- (void)unregisterCell;
- (void)addSearchControllerToTableView;

@end
