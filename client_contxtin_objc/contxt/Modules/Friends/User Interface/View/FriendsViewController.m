//
//  FriendsViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 20/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "FriendsViewController.h"
#import "SessionManager.h"
#import "FriendsTableViewManager.h"

@interface FriendsViewController ()

@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) FriendsTableViewManager *tableViewManager;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

@implementation FriendsViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil sessionManager:(id)sessionManager {
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        _sessionManager = sessionManager;
        _tableViewManager = [[FriendsTableViewManager alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Contxt";
    self.tableViewManager.sessionManager = self.sessionManager;
    self.tableViewManager.tableView = self.tableView;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.definesPresentationContext = YES;
    [self.tableViewManager addSearchControllerToTableView];
    [self loadInitialView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.tableViewManager unregisterCell];
    [super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.tableViewManager.sessionManager = self.sessionManager;
    
}

- (void)loadInitialView{
    self.tableViewManager.tableView = self.tableView;
    self.tableViewManager.friendsViewController = self;
}


@end
