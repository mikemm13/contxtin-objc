//
//  FriendsTableViewManager.m
//  contxt
//
//  Created by Miguel Martin Nieto on 20/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "FriendsTableViewManager.h"
#import "Friend.h"
#import "FriendsInteractor.h"
#import "FriendsViewController.h"

@interface FriendsTableViewManager ()
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) FriendsInteractor *friendsInteractor;
@property (strong, nonatomic) NSOperationQueue *searchQueue;
@property (strong, nonatomic) NSMutableArray *searchResults;
@end

@implementation FriendsTableViewManager

- (void)setTableView:(UITableView *)tableView{
    _tableView = tableView;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.allowsMultipleSelection = YES;
    _tableView.contentOffset = CGPointMake(0, 44);
    [_tableView reloadData];
}

- (void)addSearchControllerToTableView {
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    [self.searchController.searchBar sizeToFit];
    [self.tableView setContentOffset:CGPointMake(0, self.searchController.searchBar.frame.size.height) animated:NO];
    self.tableView.tableHeaderView = self.searchController.searchBar;

}

- (FriendsInteractor *)friendsInteractor {
    if (!_friendsInteractor) {
        _friendsInteractor = [[FriendsInteractor alloc] initWithSessionManager:self.sessionManager];
    }
    return _friendsInteractor;
}

- (NSMutableArray *)searchResults {
    if (!_searchResults) {
        _searchResults = [NSMutableArray array];
    }
    return _searchResults;
}

- (NSOperationQueue *)searchQueue {
    if (!_searchQueue) {
        _searchQueue = [[NSOperationQueue alloc] init];
        _searchQueue.maxConcurrentOperationCount = 1;
    }
    return _searchQueue;
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ in friendOfUser", self.sessionManager.userLogged];
    
    _fetchedResultsController = [Friend MR_fetchAllSortedBy:@"username" ascending:YES withPredicate:predicate groupBy:nil delegate:self];
    
    return _fetchedResultsController;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count;
    if (self.searchController.active) {
        count = self.searchResults.count;
    } else {
        count=[[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static BOOL friendCellNibRegistered = NO;
    NSString *cellClass = NSStringFromClass([FriendsTableViewCell class]);
    if (!friendCellNibRegistered) {
        [tableView registerNib:[UINib nibWithNibName:cellClass bundle:nil] forCellReuseIdentifier:cellClass];
        friendCellNibRegistered = YES;
    }
    FriendsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellClass forIndexPath:indexPath];
    cell.sessionManager = self.sessionManager;
    cell.delegate = self;
    Friend *friend;
    if (self.searchController.active) {
        friend = [self.searchResults objectAtIndex:indexPath.row];
    } else {
        friend = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    cell.friendUserNameLabel.text = friend.username;
    cell.friendId = friend.uid;
    NSArray *friendsIds = [[self.sessionManager.userLogged.friends valueForKey:@"uid"] allObjects];
    if ([friendsIds containsObject:friend.uid]) {
        [cell.addOrRemoveFriendButton setImage:[UIImage imageNamed:@"deleteIcon"] forState:UIControlStateNormal];
        cell.addOrRemoveFriendButton.tintColor = [UIColor redColor];
    } else {
        [cell.addOrRemoveFriendButton setImage:[UIImage imageNamed:@"addIcon"] forState:UIControlStateNormal];
        if (friend.invited) {
            cell.addOrRemoveFriendButton.tintColor = [UIColor blueColor];
        } else {
            cell.addOrRemoveFriendButton.tintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"addIcon"]];            
        }
    }
    return cell;
}

- (void)unregisterCell{
    
}

#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableViewRowAnimation animation = UITableViewRowAnimationFade;
    NSIndexPath *changeIndexPath=indexPath;
    NSIndexPath *newChangeIndexPath=newIndexPath;
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newChangeIndexPath] withRowAnimation:animation];
            break;
        case NSFetchedResultsChangeMove:
            if ([self.tableView respondsToSelector:@selector(moveRowAtIndexPath:toIndexPath:)]) {
                [self.tableView moveRowAtIndexPath:changeIndexPath toIndexPath:newChangeIndexPath];
            } else {
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:changeIndexPath] withRowAnimation:animation];
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:newChangeIndexPath.section] withRowAnimation:animation];
            }
            break;
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:changeIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:changeIndexPath] withRowAnimation:animation];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    NSIndexSet *indexSetWithSectionIndex = [NSIndexSet indexSetWithIndex:sectionIndex];
    
    UITableViewRowAnimation animation = UITableViewRowAnimationFade;
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:indexSetWithSectionIndex withRowAnimation:animation];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:indexSetWithSectionIndex withRowAnimation:animation];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.tableView endUpdates];
}

#pragma mark - Search delegate methods

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    if  ([searchString isEqualToString:@""] == NO)
    {
        [self.searchQueue cancelAllOperations];
        @weakify(self)
        [self.searchQueue addOperationWithBlock:^{
            @strongify(self)
            [self.friendsInteractor searchFriendsWithName:searchString completion:^(NSArray *fetchedResults) {
                NSArray *results = fetchedResults;
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self.searchResults removeAllObjects];
                    [self.searchResults addObjectsFromArray:results];
                    
                    [self.tableView reloadData];
                }];
            }];
        }];
        
    }
    else
    {
        [self.searchResults removeAllObjects];
        [self.tableView reloadData];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.searchController.active = NO;
    [self.tableView reloadData];
}

#pragma mark - FriendsTableViewCellDelegate

- (void)presentAlertController:(UIAlertController *)alertController {
    [self.friendsViewController presentViewController:alertController animated:NO completion:^{
        
    }];
}

@end
