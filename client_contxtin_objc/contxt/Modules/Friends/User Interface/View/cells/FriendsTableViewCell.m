//
//  FriendsTableViewCell.m
//  contxt
//
//  Created by Miguel Martin Nieto on 20/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "FriendsTableViewCell.h"
#import "FriendsInteractor.h"

@interface FriendsTableViewCell ()

@property (nonatomic, strong) FriendsInteractor *friendsInteractor;

@end

@implementation FriendsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

- (FriendsInteractor *)friendsInteractor {
    if (!_friendsInteractor) {
        _friendsInteractor = [[FriendsInteractor alloc] initWithSessionManager:self.sessionManager];
    }
    return _friendsInteractor;
}

- (IBAction)addOrRemoveButtonPressed:(id)sender {
    if ([[self.addOrRemoveFriendButton imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"addIcon"]]) {
        @weakify(self)
        [self.friendsInteractor addFriendWithId:self.friendId completion:^{
            @strongify(self)
            self.addOrRemoveFriendButton.tintColor = [UIColor blueColor];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"User invited" message:@"The person has a notification now" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {}];
            [alertController addAction:okAction];
            [self.delegate presentAlertController:alertController];
        } error:^(NSString *errorCode) {
            @strongify(self)
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"User invited" message:@"You have already sent an invitation to this user" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {}];
            [alertController addAction:okAction];
            [self.delegate presentAlertController:alertController];
        }];
    } else {
        [self.friendsInteractor removeFriendWithId:self.friendId completion:^{
            
        }];
    }
}

@end
