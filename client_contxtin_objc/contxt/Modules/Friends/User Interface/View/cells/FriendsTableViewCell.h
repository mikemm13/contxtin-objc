//
//  FriendsTableViewCell.h
//  contxt
//
//  Created by Miguel Martin Nieto on 20/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"

@protocol FriendsTableViewCellDelegate <NSObject>

- (void)presentAlertController:(UIAlertController *)alertController;

@end

@interface FriendsTableViewCell : UITableViewCell
@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) NSString *friendId;
@property (weak, nonatomic) IBOutlet UILabel *friendUserNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *addOrRemoveFriendButton;
@property (weak, nonatomic) id<FriendsTableViewCellDelegate> delegate;

@end
