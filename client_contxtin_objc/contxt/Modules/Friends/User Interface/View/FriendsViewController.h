//
//  FriendsViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 20/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsViewController : UIViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil sessionManager:(id)sessionManager;

@end
