//
//  CloudsPageViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"
#import "CloudDataViewController.h"
#import "CloudPageViewControllerDatasource.h"

@interface CloudsPageViewController : UIPageViewController<CloudPageViewControllerDatasourceDelegate>
@property (strong, nonatomic) SessionManager *sessionManager;
@property (copy, nonatomic) NSString *categoryName;
@property (strong, nonatomic) CloudDataViewController *startingViewController;
@property (strong, nonatomic) CloudPageViewControllerDatasource *dataSourceController;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil categoryName:(NSString *)categoryName sessionManager:(SessionManager *)sessionManager;

@end
