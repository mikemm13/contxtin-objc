//
//  CommentCloudViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 28/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"

@protocol CommentCloudViewControllerDelegate <NSObject>

- (void)dismissCommentScreen;

@end

@interface CommentCloudViewController : UIViewController<UITextViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) id<CommentCloudViewControllerDelegate> delegate;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil cloudId:(NSString *)cloudId sessionManager:(SessionManager *)sessionManager;

@end
