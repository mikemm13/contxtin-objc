//
//  CloudsRootViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "CloudsRootViewController.h"
#import "CloudsPageViewController.h"
#import "CloudsInteractor.h"
#import "Cloud.h"


@interface CloudsRootViewController ()
@property (strong, nonatomic) SessionManager *sessionManager;
@property (copy, nonatomic) NSString *categoryName;
@property (weak, nonatomic) IBOutlet UIToolbar *actionsToolbar;
@property (weak, nonatomic) IBOutlet UIView *pageViewControllerContainer;
@property (strong, nonatomic) CloudsInteractor *cloudsInteractor;
@property (nonatomic) BOOL isSavedScreen;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (strong, nonatomic) UINavigationController *modalNavigationController;
@end

@implementation CloudsRootViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil categoryName:(NSString *)categoryName sessionManager:(SessionManager *)sessionManager isSavedScreen:(BOOL)isSavedScreen {
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        _categoryName = categoryName;
        _sessionManager = sessionManager;
        _isSavedScreen = isSavedScreen;
    }
    return self;
}

- (CloudsInteractor *)cloudsInteractor{
    if (!_cloudsInteractor) {
        _cloudsInteractor = [[CloudsInteractor alloc] initWithSessionManager:self.sessionManager];
    }
    return _cloudsInteractor;
}

- (UINavigationController *)modalNavigationController {
    if (!_modalNavigationController) {
        _modalNavigationController = [[UINavigationController alloc] init];
        _modalNavigationController.navigationBarHidden = YES;
        
    }
    return _modalNavigationController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (self.isSavedScreen) {
        self.title = @"Saved clouds";
        self.saveButton.image = [UIImage imageNamed:@"delete"];
    } else {
        self.title = self.categoryName;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCloudsLoaded) name:@"cloudsUpdated" object:nil];
    [self loadClouds];
}

- (void)loadClouds
{
    CloudsPageViewController *cloudsPageViewController = [[CloudsPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    cloudsPageViewController.categoryName = self.categoryName;
    cloudsPageViewController.sessionManager = self.sessionManager;
    cloudsPageViewController.view.frame = self.pageViewControllerContainer.bounds;
    [self.pageViewControllerContainer addSubview:cloudsPageViewController.view];
    [self addChildViewController:cloudsPageViewController];
}


- (void)updateCloudsLoaded {
    for (UIView *view in self.pageViewControllerContainer.subviews) {
        [view removeFromSuperview];
    }
    for (UIViewController *viewController in self.childViewControllers) {
        [viewController removeFromParentViewController];
    }
    [self loadClouds];
}

- (IBAction)saveButtonPressed:(id)sender {
    CloudsPageViewController *cloudsPageViewController = self.childViewControllers[0];
    NSInteger cloudIndex = cloudsPageViewController.startingViewController.index;
    CloudDataViewController *cloudDataViewController = [cloudsPageViewController.dataSourceController viewControllerAtIndex:cloudIndex];
    Cloud *cloud = (Cloud *)cloudDataViewController.dataObject;
    NSString *cid = cloud.cid;
    Cloud *cloudInContext = [Cloud MR_findFirstByAttribute:@"cid" withValue:cid inContext:[NSManagedObjectContext MR_defaultContext]];
    if (self.isSavedScreen) {
        @weakify(self)
        [self.cloudsInteractor unsaveCloudWithId:cid completion:^(BOOL unsaveSucceeded, NSString *message) {
            @strongify(self)
            if (unsaveSucceeded) {
                cloudInContext.saved = @0;
                [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
                    [self updateCloudsLoaded];
                }];
            }
        }];

    } else {
        [self.cloudsInteractor saveCloudWithId:cid completion:^(BOOL saveSucceeded, NSString *message) {
            if (saveSucceeded) {
                cloudInContext.saved = @1;
                [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
                    
                }];
            }
        }];
    }
}

- (IBAction)commentButtonPressed:(id)sender {
    CloudsPageViewController *cloudsPageViewController = self.childViewControllers[0];
    NSInteger cloudIndex = cloudsPageViewController.startingViewController.index;
    CloudDataViewController *cloudDataViewController = [cloudsPageViewController.dataSourceController viewControllerAtIndex:cloudIndex];
    Cloud *cloud = (Cloud *)cloudDataViewController.dataObject;
    NSString *cid = cloud.cid;
    [self.view addSubview:self.modalNavigationController.view];
    CommentCloudViewController *commentCloudViewController = [[CommentCloudViewController alloc] initWithNibName:@"CommentCloudViewController" cloudId:cid sessionManager:self.sessionManager];
    commentCloudViewController.delegate = self;
    [self.modalNavigationController presentViewController:commentCloudViewController animated:YES completion:NULL];
}

- (void)dismissCommentScreen {
    [self.modalNavigationController dismissViewControllerAnimated:YES completion:^{
        [self.modalNavigationController.view removeFromSuperview];
    }];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
