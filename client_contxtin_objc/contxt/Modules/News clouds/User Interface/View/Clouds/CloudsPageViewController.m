//
//  CloudsPageViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "CloudsPageViewController.h"
#import "CloudsWireFrame.h"


@interface CloudsPageViewController ()

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation CloudsPageViewController


- (instancetype)initWithNibName:(NSString *)nibNameOrNil categoryName:(NSString *)categoryName sessionManager:(SessionManager *)sessionManager{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        _categoryName = categoryName;
        _sessionManager = sessionManager;
        
    }
    return self;
}


- (void)viewDidLoad{
    [super viewDidLoad];
    [self.view addSubview:self.activityIndicatorView];
    [self.view bringSubviewToFront:self.activityIndicatorView];
    [self loadViewControllers];
}

- (UIActivityIndicatorView *)activityIndicatorView {
    if (!_activityIndicatorView) {
        _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _activityIndicatorView.color = [UIColor colorWithRed:0/255 green:128/255 blue:255/255 alpha:1];
        _activityIndicatorView.hidesWhenStopped = YES;
        _activityIndicatorView.center = CGPointMake(self.view.center.x, self.view.center.y - 44);
    }
    return _activityIndicatorView;
}

- (void)loadViewControllers {
    CloudDataViewController *firstViewController = self.startingViewController;
    NSArray *viewControllers = [NSArray array];
    
    if (!firstViewController) {
        self.activityIndicatorView.hidden = NO;
        [self.activityIndicatorView startAnimating];
    } else {
        [self.activityIndicatorView stopAnimating];
        viewControllers = @[firstViewController];
        [self setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        self.dataSource = self.dataSourceController;
    }
}

- (CloudDataViewController *)startingViewController{
    if (!_startingViewController) {
        _startingViewController = [self.dataSourceController viewControllerAtIndex:0 ];
    }
    return _startingViewController;
}

- (CloudPageViewControllerDatasource *)dataSourceController{
    if (!_dataSourceController) {
        _dataSourceController = [[CloudPageViewControllerDatasource alloc] init];
        _dataSourceController.sessionManager = self.sessionManager;
        _dataSourceController.categoryName = self.categoryName;
        _dataSourceController.delegate = self;
        [_dataSourceController addObserverIfNeeded];
    }
    return _dataSourceController;
}

- (void)updateViewControllers {
    [self loadViewControllers];
}

@end
