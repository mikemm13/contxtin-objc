//
//  ModelController.h
//  testPageVC
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"
@class CloudDataViewController;

@protocol CloudPageViewControllerDatasourceDelegate <NSObject>

- (void) updateViewControllers;

@end

@interface CloudPageViewControllerDatasource : NSObject <UIPageViewControllerDataSource>

@property (copy, nonatomic) NSString *categoryName;
@property (strong, nonatomic) SessionManager *sessionManager;
@property (weak, nonatomic) id<CloudPageViewControllerDatasourceDelegate> delegate;

- (CloudDataViewController *)viewControllerAtIndex:(NSUInteger)index;
- (NSUInteger)indexOfViewController:(CloudDataViewController *)viewController;
- (void)addObserverIfNeeded;

@end
