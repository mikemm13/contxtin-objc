//
//  CommentCloudViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 28/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "CommentCloudViewController.h"
#import "UITextView+Placeholder.h"
#import "Friend.h"
#import "CloudsProvider.h"

@interface CommentCloudViewController ()
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UITableView *friendsTableView;
@property (weak, nonatomic) IBOutlet UIButton *dismissButton;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionHeightConstraint;
@property (nonatomic) CGFloat currentKeyboardHeight;
@property (strong, nonatomic) NSArray *friendsList;
@property (strong, nonatomic) NSMutableArray *selectedFriends;
@property (strong, nonatomic) NSString *cloudId;
@property (strong, nonatomic) SessionManager *sessionManager;

@end

@implementation CommentCloudViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil cloudId:(NSString *)cloudId sessionManager:(SessionManager *)sessionManager {
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        _cloudId = cloudId;
        _sessionManager = sessionManager;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.commentTextView.delegate = self;
    self.commentTextView.placeholder = @"Write a comment";
    self.commentTextView.placeholderColor = [UIColor lightGrayColor];
    self.friendsList = [Friend MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]];
    self.selectedFriends = [NSMutableArray array];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    self.friendsTableView.delegate = self;
    self.friendsTableView.dataSource = self;
    [self.friendsTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"FriendCell"];
}



- (void)textViewDidChange:(UITextView *)textView {
    CGFloat maxHeight = [UIScreen mainScreen].bounds.size.height - self.currentKeyboardHeight - textView.frame.origin.y;
    if (textView.contentSize.height < maxHeight) {
        self.descriptionHeightConstraint.constant = textView.contentSize.height;
        [self.view layoutIfNeeded];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)dismissButtonPressed:(id)sender {
    [self.delegate dismissCommentScreen];
}

- (IBAction)commentButtonPressed:(id)sender {
    UIAlertController *alertController;
    if (self.commentTextView.text.length == 0) {
        alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"You need to write something to send" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:NULL]];
        [self presentViewController:alertController animated:YES completion:NULL];
    } else if (!self.selectedFriends.count){
        alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Select at least one friend" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:NULL]];
        [self presentViewController:alertController animated:YES completion:NULL];
    } else {
        CloudsProvider *cloudsProvider = [[CloudsProvider alloc] init];
        cloudsProvider.sessionManager = self.sessionManager;
        [cloudsProvider commentCloudWithId:self.cloudId toFriends:self.selectedFriends message:self.commentTextView.text successBlock:^(id data) {
            [self.delegate dismissCommentScreen];            
        } errorBlock:^(NSError *error) {
            
        }];
    }
}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    self.currentKeyboardHeight = kbSize.height;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark - TableView methods

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"TO:";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.friendsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FriendCell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FriendCell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    Friend *friend = [self.friendsList objectAtIndex:indexPath.row];
    cell.textLabel.text = friend.username;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    Friend *selectedFriend = [self.friendsList objectAtIndex:indexPath.row];
    if (selectedCell.accessoryType == UITableViewCellAccessoryNone) {
        selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.selectedFriends addObject:selectedFriend.uid];
    } else {
        selectedCell.accessoryType = UITableViewCellAccessoryNone;
        [self.selectedFriends removeObject:selectedFriend.uid];
    }
}

@end
