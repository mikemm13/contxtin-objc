//
//  CloudDataViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "CloudDataViewController.h"
#import "Cloud.h"
#import "Ranking.h"
#import "RankingElementsTableViewManager.h"
#import "CloudRankingTableViewCell.h"
#import "Tag.h"

@interface CloudDataViewController ()

@property (weak, nonatomic) IBOutlet UILabel *cloudIndex;
@property (weak, nonatomic) IBOutlet UILabel *totalClouds;
@property (weak, nonatomic) IBOutlet UITableView *rankingElementsTableView;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *tagLabels;
@property (strong, nonatomic) RankingElementsTableViewManager *tableViewManager;

@end

@implementation CloudDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    

    self.rankingElementsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.cloudIndex.text =  [NSString stringWithFormat:@"%li", self.index+1];
    self.totalClouds.text = [NSString stringWithFormat:@"%li", (long)self.numberOfCloudsInCategory];
    Cloud *cloud = self.dataObject;
    
    NSArray *tags = [cloud.tags allObjects];
    NSUInteger index = 0;
    for (UILabel *tagLabel in self.tagLabels) {
        
        if (index < tags.count) {
            tagLabel.text = [(Tag *)[tags objectAtIndex:index] name];
        } else {
            tagLabel.text = @"";
        }
        index ++;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tableViewManager.sessionManager = self.sessionManager;
    self.tableViewManager.cloudPresented = self.dataObject;
    self.tableViewManager.tableView = self.rankingElementsTableView;
}

- (void)loadRankingElements{
    
    self.tableViewManager.router = self.router;
}

- (RankingElementsTableViewManager *)tableViewManager{
    if (!_tableViewManager) {
        _tableViewManager = [[RankingElementsTableViewManager alloc] init];
    }
    return _tableViewManager;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
