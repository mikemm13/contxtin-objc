//
//  CloudsRootViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"
#import "CommentCloudViewController.h"

@interface CloudsRootViewController : UIViewController<CommentCloudViewControllerDelegate>
- (instancetype)initWithNibName:(NSString *)nibNameOrNil categoryName:(NSString *)categoryName sessionManager:(SessionManager *)sessionManager isSavedScreen:(BOOL)isSavedScreen;
@end
