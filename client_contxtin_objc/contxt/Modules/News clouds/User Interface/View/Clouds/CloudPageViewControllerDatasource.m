//
//  ModelController.m
//  testPageVC
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "CloudPageViewControllerDatasource.h"

#import "CloudDataViewController.h"
#import "ContxtCategory.h"
#import "CloudsWireFrame.h"
#import "Cloud.h"


@interface CloudPageViewControllerDatasource()
@property (strong, nonatomic) NSArray *clouds;

@end

@implementation CloudPageViewControllerDatasource

- (id)init
{
    self = [super init];
    if (self) {
        
        
    }
    return self;
}

- (void)addObserverIfNeeded {
    if (!self.categoryName) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDataViewControllerWhenCloudsLoaded) name:@"cloudsUpdated" object:nil];        
    }
}

- (NSArray *)clouds{
    NSArray *cloudsList;
    if (self.categoryName) {
        ContxtCategory *category = [ContxtCategory MR_findFirstByAttribute:@"category" withValue:self.categoryName inContext:[NSManagedObjectContext MR_defaultContext]];
        cloudsList = [category.clouds allObjects];
    } else {
        cloudsList = [Cloud MR_findByAttribute:@"saved" withValue:@1 inContext:[NSManagedObjectContext MR_defaultContext]];
    }
    return cloudsList;
}

- (CloudDataViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.clouds count] == 0) || (index >= [self.clouds count])) {
        return nil;
    }
    
    CloudDataViewController *dataViewController = [[CloudDataViewController alloc] initWithNibName:@"CloudDataViewController" bundle:nil];
    CloudsWireFrame *router = [[CloudsWireFrame alloc] initWithCloudDataViewController:dataViewController];
    
    dataViewController.index = index;
    dataViewController.numberOfCloudsInCategory = self.clouds.count;
    dataViewController.sessionManager = self.sessionManager;
    dataViewController.dataObject = self.clouds[index];
    dataViewController.router = router;
    [dataViewController loadRankingElements];
    
    return dataViewController;
}

- (void) updateDataViewControllerWhenCloudsLoaded {
    [self.delegate updateViewControllers];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSUInteger)indexOfViewController:(CloudDataViewController *)viewController
{   
    
    return [self.clouds indexOfObject:viewController.dataObject];
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(CloudDataViewController *)viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(CloudDataViewController *)viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.clouds count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

@end
