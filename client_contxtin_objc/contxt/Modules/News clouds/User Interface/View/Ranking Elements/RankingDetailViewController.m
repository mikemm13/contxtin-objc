//
//  RankingDetailViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 24/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "RankingDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+HTML.h"
#import "WebViewViewController.h"

@interface RankingDetailViewController ()
@property (strong, nonatomic) Ranking *rankingElement;
@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) CloudsWireFrame *router;
@property (weak, nonatomic) IBOutlet UILabel *rankingTitle;

@property (weak, nonatomic) IBOutlet UILabel *sourceName;
@property (weak, nonatomic) IBOutlet UILabel *publishDate;
@property (weak, nonatomic) IBOutlet UILabel *readingTime;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *cleanBody;
@property (strong, nonatomic) IBOutlet UIWebView *cloudWebView;

@end

@implementation RankingDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil ranking:(Ranking *)rankingElement sessionManager:(SessionManager *)sessionManager router:(CloudsWireFrame *)router
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        _rankingElement = rankingElement;
        _sessionManager = sessionManager;
        _router = router;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.rankingTitle.text = self.rankingElement.title;
    self.sourceName.text = self.rankingElement.sourceName;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMMM yyyy, hh:mm"];
    
    self.publishDate.text = [formatter stringFromDate:self.rankingElement.publishDate];
    
    self.readingTime.text = self.rankingElement.readingTime;
    self.cleanBody.text = [self.rankingElement.body stringByConvertingHTMLToPlainText];
    NSURL *url = [NSURL URLWithString:self.rankingElement.image];
    [self.image sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onPressedVisitWebsite:(id)sender {
    NSURL *url = [NSURL URLWithString:self.rankingElement.link];
    WebViewViewController *webViewVC = [[WebViewViewController alloc] initWithNibName:@"WebViewViewController" url:url title:self.rankingElement.sourceName];
    [self.router loadSelectedItemContent:webViewVC];
}

@end
