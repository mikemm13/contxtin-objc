//
//  CloudRankingCellDrawer.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RankingCellDrawerProtocol.h"

@interface CloudRankingCellDrawer : NSObject<RankingCellDrawerProtocol>

@end
