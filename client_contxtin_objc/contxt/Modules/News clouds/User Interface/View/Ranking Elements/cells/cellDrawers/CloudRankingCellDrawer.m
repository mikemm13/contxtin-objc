//
//  CloudRankingCellDrawer.m
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "CloudRankingCellDrawer.h"
#import "Ranking.h"
#import "CloudRankingTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation CloudRankingCellDrawer
static BOOL categoryCellNibRegistered = NO;
- (instancetype)init
{
    self = [super init];
    if (self) {
        categoryCellNibRegistered = NO;
    }
    return self;
}
- (void)registerCellNibInTableViewIfNeeded:(UITableView *)tableView{
    if (!categoryCellNibRegistered) {
        NSString *cellClass = NSStringFromClass([CloudRankingTableViewCell class]);
        [tableView registerNib:[UINib nibWithNibName:cellClass bundle:nil] forCellReuseIdentifier:cellClass];
        categoryCellNibRegistered = YES;
    }
}
- (UITableViewCell *)cellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath{
    [self registerCellNibInTableViewIfNeeded:tableView];
    return [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CloudRankingTableViewCell class]) forIndexPath:indexPath];
}
- (void)drawCell:(CloudRankingTableViewCell *)cell withItem:(Ranking *)item{
    cell.title.text = item.title;
    [cell.title sizeToFit];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMMM yyyy, hh:mm"];
    
    cell.dateOfPublish.text = [formatter stringFromDate:item.publishDate];
    
    cell.feedSource.text = item.sourceName;
    NSURL *imageURL = [NSURL URLWithString:item.image];
    [cell.image sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
}
@end
