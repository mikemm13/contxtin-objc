//
//  CloudRankingTableViewCell.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CloudRankingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *dateOfPublish;
@property (weak, nonatomic) IBOutlet UILabel *feedSource;

@end
