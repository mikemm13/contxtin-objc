//
//  RankingCellDrawerProtocol.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RankingItemProtocol;
@protocol RankingCellDrawerProtocol <NSObject>
- (UITableViewCell *)cellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
- (void)drawCell:(UITableViewCell *)cell withItem:(id<RankingItemProtocol>)item;
@end
