//
//  WebViewViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 8/5/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "WebViewViewController.h"

@interface WebViewViewController ()

@property (strong, nonatomic) NSURL *url;
@property (copy, nonatomic) NSString *webViewTitle;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation WebViewViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil url:(NSURL *)url title:(NSString *)title
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        _url = url;
        _webViewTitle = title;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.delegate = self;
    self.title = self.webViewTitle;
    [self.activityIndicator startAnimating];
    [self.webView loadRequest:[[NSURLRequest alloc] initWithURL:self.url]];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.activityIndicator stopAnimating];
}


@end
