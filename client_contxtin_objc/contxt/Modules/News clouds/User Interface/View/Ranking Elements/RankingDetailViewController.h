//
//  RankingDetailViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 24/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ranking.h"
#import "SessionManager.h"
#import "CloudsWireFrame.h"

@interface RankingDetailViewController : UIViewController

- (instancetype)initWithNibName:(NSString*)nibNameOrNil ranking:(Ranking *)rankingElement sessionManager:(SessionManager *)sessionManager router:(CloudsWireFrame *)router;

@end
