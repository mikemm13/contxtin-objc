//
//  RankingElementsTableViewManager.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"
#import "Cloud.h"
#import "CloudsWireFrame.h"

@interface RankingElementsTableViewManager : NSObject<NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) Cloud *cloudPresented;
@property (strong, nonatomic) CloudsWireFrame *router;
- (void)loadFetchedResultController;
@end
