//
//  WebViewViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 8/5/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewViewController : UIViewController<UIWebViewDelegate>

-(instancetype)initWithNibName:(NSString *)nibNameOrNil url:(NSURL *)url title:(NSString *)title;

@end
