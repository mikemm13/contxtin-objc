//
//  RankingItemProtocol.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RankingCellDrawerProtocol.h"
#import "SessionManager.h"
#import "CloudsWireFrame.h"

@protocol RankingItemProtocol <NSObject>
@property (copy,nonatomic,readonly) NSString *itemTitle;
@property (strong,nonatomic,readonly) id<RankingCellDrawerProtocol> cellDrawer;
- (UIViewController *)contentsViewControllerWithSessionManager:(SessionManager *)sessionManager router:(CloudsWireFrame *)router;
@end
