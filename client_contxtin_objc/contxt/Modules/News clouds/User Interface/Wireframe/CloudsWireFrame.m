//
//  CloudsWireFrame.m
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "CloudsWireFrame.h"

@interface CloudsWireFrame()

@property (strong, nonatomic)CloudDataViewController *cloudData;
@end

@implementation CloudsWireFrame

- (instancetype)initWithCloudDataViewController:(CloudDataViewController *)cloudData
{
    self = [super init];
    if (self) {
        _cloudData = cloudData;
        
    }
    return self;
}

- (void)loadSelectedItemContent:(UIViewController *)contentsViewController; {
    
    [self.cloudData.navigationController pushViewController:contentsViewController animated:YES];
}


@end
