//
//  CloudsWireFrame.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CloudDataViewController.h"

@interface CloudsWireFrame : NSObject<CloudDataViewControllerRouter>
@property (strong, nonatomic)SessionManager *sessionManager;
- (instancetype)initWithCloudDataViewController:(CloudDataViewController *)cloudData;
- (void)loadSelectedItemContent:(UIViewController *)contentsViewController;

@end
