//
//  CategoriesTableViewController.m
//  contxt
//
//  Created by Miguel Martin Nieto on 26/7/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "CategoriesTableViewController.h"
#import "CategoryTableViewManager.h"
#import "CategoriesInteractor.h"
#import "ContxtCategory.h"
#import "SignUpWireFrame.h"

@interface CategoriesTableViewController ()

@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) CategoryTableViewManager *tableViewManager;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@end

@implementation CategoriesTableViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil sessionManager:(id)sessionManager {
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        _sessionManager = sessionManager;
        _tableViewManager = [[CategoryTableViewManager alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Choose categories";
    [self.saveButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [self loadInitialView];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.tableViewManager unregisterCell];
    [super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.tableViewManager.sessionManager = self.sessionManager;
    self.tableViewManager.tableView = self.tableView;
}

- (void)loadInitialView{
    self.tableViewManager.tableView = self.tableView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)saveButtonPressed:(id)sender {
    CategoriesInteractor *categoriesInteractor = [[CategoriesInteractor alloc] initWithSessionManager:self.sessionManager];
    __block NSArray *selectedCategories = [self.tableViewManager getSelectedCategories];
    @weakify(self)
    [categoriesInteractor updateCategories:selectedCategories completion:^{
        @strongify(self)
        for (ContxtCategory *category in [ContxtCategory MR_findAll]) {
            if ([selectedCategories containsObject:category] ) {
                category.selected = @1;
            } else {
                category.selected = @0;
            }
        }
        @weakify(self)
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            @strongify(self)
            if ([self.presentingViewController isKindOfClass:[UINavigationController class]]) {
                [self dismissViewControllerAnimated:NO completion:^{
                }];
                [self.presentingViewController dismissViewControllerAnimated:NO completion:^{
                    
                }];
            } else {
                //SAVED message
            }
        }];
    }];
}


@end
