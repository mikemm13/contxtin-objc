//
//  CategoryTableViewManager.m
//  contxt
//
//  Created by Miguel Martin Nieto on 26/7/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "CategoryTableViewManager.h"
#import "ContxtCategory.h"
#import "ContxtCategory+DashboardItem.h"
#import "CategoryItemProtocol.h"

@interface CategoryTableViewManager ()

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSMutableArray *selectedCategories;
@property (nonatomic) BOOL screenHasBeenSet;

@end

@implementation CategoryTableViewManager

- (void)setTableView:(UITableView *)tableView{
    _tableView = tableView;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.allowsMultipleSelection = YES;
    [_tableView reloadData];
}

- (NSMutableArray *)selectedCategories {
    if (!_selectedCategories) {
        _selectedCategories = [NSMutableArray array];
        for (id<CategoryItemProtocol> item in self.fetchedResultsController.fetchedObjects) {
            if ([item.cellDrawer selectedValueFromItem:item]) {
                [_selectedCategories addObject:item.itemTitle];
            }
        }
    }
    return _selectedCategories;
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController = [ContxtCategory MR_fetchAllSortedBy:@"category" ascending:YES withPredicate:nil groupBy:nil delegate:self];
    
    return _fetchedResultsController;
}

- (void) setCategoryScreen {
    for (ContxtCategory *contxtCategory in _fetchedResultsController.fetchedObjects) {
        contxtCategory.screenWhereCategoryIsShown = @"Categories";
    }
    self.screenHasBeenSet = YES;
}

- (void)unregisterCell {
    if ([self.tableView numberOfRowsInSection:0] > 0) {
        NSIndexPath *indexPath = [[self.tableView indexPathsForVisibleRows] firstObject];
        id<CategoryItemProtocol> item = (id<CategoryItemProtocol>)[self.fetchedResultsController objectAtIndexPath:indexPath];
        [item.cellDrawer unregisterCellNib];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count=[[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setCategoryScreen];

    id<CategoryItemProtocol> item = (id<CategoryItemProtocol>)[self.fetchedResultsController objectAtIndexPath:indexPath];
    UITableViewCell *cell = [item.cellDrawer cellForTableView:tableView atIndexPath:indexPath];
    [item.cellDrawer drawCell:cell withItem:item];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    id<CategoryItemProtocol> item = (id<CategoryItemProtocol>)[self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([item.cellDrawer selectedValueFromItem:item]) {
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];    
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    id<CategoryItemProtocol> item = (id<CategoryItemProtocol>)[self.fetchedResultsController objectAtIndexPath:indexPath];
    [self.selectedCategories addObject:item.itemTitle];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    id<CategoryItemProtocol> item = (id<CategoryItemProtocol>)[self.fetchedResultsController objectAtIndexPath:indexPath];
    [self.selectedCategories removeObject:item.itemTitle];
}

#pragma mark - Categories selection state
- (NSArray *)getSelectedCategories {
    NSMutableArray *categoriesList = [NSMutableArray array];
    for (NSString *category in self.selectedCategories) {
        ContxtCategory *contxtCategory = [ContxtCategory MR_findFirstByAttribute:@"category" withValue:category];
        [categoriesList addObject:contxtCategory];
    }
    return categoriesList.copy;
}


#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableViewRowAnimation animation = UITableViewRowAnimationFade;
    NSIndexPath *changeIndexPath=indexPath;
    NSIndexPath *newChangeIndexPath=newIndexPath;
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newChangeIndexPath] withRowAnimation:animation];
            break;
        case NSFetchedResultsChangeMove:
            if ([self.tableView respondsToSelector:@selector(moveRowAtIndexPath:toIndexPath:)]) {
                [self.tableView moveRowAtIndexPath:changeIndexPath toIndexPath:newChangeIndexPath];
            } else {
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:changeIndexPath] withRowAnimation:animation];
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:newChangeIndexPath.section] withRowAnimation:animation];
            }
            break;
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:changeIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:changeIndexPath] withRowAnimation:animation];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    NSIndexSet *indexSetWithSectionIndex = [NSIndexSet indexSetWithIndex:sectionIndex];
    
    UITableViewRowAnimation animation = UITableViewRowAnimationFade;
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:indexSetWithSectionIndex withRowAnimation:animation];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:indexSetWithSectionIndex withRowAnimation:animation];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.tableView endUpdates];
}


@end
