//
//  CategoryItemProtocol.h
//  contxt
//
//  Created by Miguel Martin Nieto on 29/7/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryCellDrawerProtocol.h"
#import "CellDrawerProtocol.h"

@protocol CategoryItemProtocol <NSObject>
@property (copy,nonatomic,readonly) NSString *itemTitle;
@property (strong,nonatomic,readonly) id<CategoryCellDrawerProtocol, CellDrawerProtocol> cellDrawer;

- (void)selectCategory;
@end
