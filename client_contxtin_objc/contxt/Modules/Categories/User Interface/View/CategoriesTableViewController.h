//
//  CategoriesTableViewController.h
//  contxt
//
//  Created by Miguel Martin Nieto on 26/7/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionManager.h"

@interface CategoriesTableViewController : UIViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil sessionManager:(SessionManager *)sessionManager;

@end
