//
//  CategoryCellDrawer.m
//  contxt
//
//  Created by Miguel Martin Nieto on 26/7/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "CategoryCellDrawer.h"
#import "CategoryTableViewCell.h"
#import "ContxtCategory.h"

@implementation CategoryCellDrawer

static BOOL categoryCellNibRegistered = NO;

- (void)registerCellNibInTableViewIfNeeded:(UITableView *)tableView{
    if (!categoryCellNibRegistered) {
        NSString *cellClass = NSStringFromClass([CategoryTableViewCell class]);
        [tableView registerNib:[UINib nibWithNibName:cellClass bundle:nil] forCellReuseIdentifier:cellClass];
        categoryCellNibRegistered = YES;
    }
}

- (void)unregisterCellNib {
    categoryCellNibRegistered = NO;
}

- (UITableViewCell *)cellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath{
    [self registerCellNibInTableViewIfNeeded:tableView];
    return [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CategoryTableViewCell class]) forIndexPath:indexPath];
}

- (void)drawCell:(CategoryTableViewCell *)cell withItem:(ContxtCategory *)item{
    cell.categoryName.text = item.category;
}

- (BOOL)selectedValueFromItem:(ContxtCategory *)item{
    return [item.selected boolValue];
}

- (void)setSelectedValueFromItem:(ContxtCategory *)item selected:(BOOL)selected {
//    item.selected = [NSNumber numberWithBool:selected];
}

@end
