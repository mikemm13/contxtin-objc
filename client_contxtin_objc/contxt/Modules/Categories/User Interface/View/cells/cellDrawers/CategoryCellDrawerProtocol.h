//
//  CategoryCellDrawerProtocol.h
//  contxt
//
//  Created by Miguel Martin Nieto on 4/8/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol CategoryItemProtocol;
@protocol CategoryCellDrawerProtocol <NSObject>
- (UITableViewCell *)cellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
- (void)drawCell:(UITableViewCell *)cell withItem:(id<CategoryItemProtocol>)item;
- (void)unregisterCellNib;
- (BOOL)selectedValueFromItem:(id<CategoryItemProtocol>)item;
- (void)setSelectedValueFromItem:(id<CategoryItemProtocol>)item selected:(BOOL)selected;
@end
