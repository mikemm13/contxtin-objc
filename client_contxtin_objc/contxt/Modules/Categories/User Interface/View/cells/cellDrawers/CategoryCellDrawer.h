//
//  CategoryCellDrawer.h
//  contxt
//
//  Created by Miguel Martin Nieto on 26/7/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryCellDrawerProtocol.h"
#import "CellDrawerProtocol.h"

@interface CategoryCellDrawer : NSObject<CategoryCellDrawerProtocol, CellDrawerProtocol>

@end
