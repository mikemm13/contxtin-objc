//
//  CategoryTableViewManager.h
//  contxt
//
//  Created by Miguel Martin Nieto on 26/7/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"

@interface CategoryTableViewManager : NSObject<NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) SessionManager *sessionManager;
- (void)loadFetchedResultController;
- (void)unregisterCell;
- (NSArray *) getSelectedCategories;

@end
