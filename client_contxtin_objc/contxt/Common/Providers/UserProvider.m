//
//  UserProvider.m
//  contxt
//
//  Created by Miguel Martin Nieto on 19/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "UserProvider.h"

@implementation UserProvider

- (User *)loadUser{
    //TODO: load User from keychain
    return [User MR_findFirstInContext:[NSManagedObjectContext MR_defaultContext]];
}

@end
