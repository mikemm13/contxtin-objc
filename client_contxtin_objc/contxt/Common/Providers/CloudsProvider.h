//
//  CloudsProvider.h
//  contxt
//
//  Created by Miguel Martin Nieto on 22/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "BaseProvider.h"
#import "SessionManager.h"

@interface CloudsProvider : BaseProvider
@property (strong,nonatomic) SessionManager *sessionManager;
- (void)cloudsWithSuccessBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock;
- (void)saveCloudWithId:(NSString *)cid successBlock:(RequestManagerSuccess)saveSuccessBlock errorBlock:(RequestManagerError)saveErrorBlock;
- (void)unsaveCloudWithId:(NSString *)cid successBlock:(RequestManagerSuccess)unsaveSuccessBlock errorBlock:(RequestManagerError)unsaveErrorBlock;
- (void)commentCloudWithId:(NSString *)cid toFriends:(NSArray *)friends message:(NSString *)message successBlock:(RequestManagerSuccess)commentSuccessBlock errorBlock:(RequestManagerError)commentErrorBlock;
- (void)addCommentToDialogue:(NSString *)dialogueId participants:(NSArray *)participants comment:(NSString *)comment successBlock:(RequestManagerSuccess)commentSuccessBlock errorBlock:(RequestManagerError)commentErrorBlock;
- (void)discardCloudWithDialogueId:(NSString *)dialogueId successBlock:(RequestManagerSuccess)discardSuccessBlock errorBlock:(RequestManagerError)discardErrorBlock;
@end
