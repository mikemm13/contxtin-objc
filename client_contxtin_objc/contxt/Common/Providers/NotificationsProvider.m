//
//  NotificationsProvider.m
//  contxt
//
//  Created by Miguel Martin Nieto on 27/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "NotificationsProvider.h"
#import "Notification.h"
#import "NSManagedObject+JSON.h"

@implementation NotificationsProvider

- (void)requestNotifications{
    NSString *path = @"user/get_notifications_and_social";
    User *user = self.sessionManager.userLogged;
    NSDictionary *parameters=@{@"uid":user.uid,
                               @"password":user.password};
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        if ([[data objectForKey:@"status"] isEqualToString:@"ok"]) {
            NSArray *notifications = [[data objectForKey:@"data"] objectForKey:@"notifications"];
            if (notifications.count) {
                __block NSInteger notificationsSaved = 0;
                for (NSDictionary *notificationDictionary in notifications) {
                    NSString *notificationType = [[notificationDictionary valueForKey:@"payload"] valueForKey:@"type"];
                    NSString *sourceName = [notificationDictionary valueForKey:@"sourceName" ];
                    NSString *source = [notificationDictionary valueForKey:@"source"];
                    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %@ and sourceName = %@", notificationType, sourceName];
                        Notification *notification = [Notification MR_findFirstWithPredicate:predicate inContext:[NSManagedObjectContext MR_defaultContext]];
                        if (!notification) {
                            notification = [Notification MR_createEntityInContext:localContext];
                            notification.type = notificationType;
                            notification.sourceName = sourceName;
                            if ([notification.type isEqualToString:@"accept_friend"]) {
                                notification.message = [NSString stringWithFormat:@"%@ accepted your friend request", notification.sourceName];
                                notification.elementId = source;
                            } else if ([notification.type isEqualToString:@"req_friend"]) {
                                notification.message = [NSString stringWithFormat:@"%@ sent you a friend request", notification.sourceName];
                                notification.elementId = source;
                            }
                            User *userInContext = [user MR_inContext:localContext];
                            [userInContext addNotificationsObject:notification];
                        }
                    } completion:^(BOOL contextDidSave, NSError *error) {
                        notificationsSaved += 1;
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %@ and sourceName = %@", notificationType, sourceName];
                        Notification *notification = [Notification MR_findFirstWithPredicate:predicate inContext:[NSManagedObjectContext MR_defaultContext]];
                        notification.user = user;
                        if (notificationsSaved == notifications.count) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationsUpdated" object:nil];
                        }
                    }];
                }
                
            }
            
        }
    } errorBlock:^(NSError *error) {
        
    }];
}

- (void)clearAllNotifications:(NSArray *)notificationsList {
    NSArray *notificationsToConvert = [self getNotificationsPreparedForJSON:notificationsList];
    User *user = self.sessionManager.userLogged;
    NSMutableDictionary *parameters=[NSMutableDictionary dictionaryWithObjectsAndKeys:user.uid, @"uid", user.password, @"password", nil];
    for (NSDictionary *notificationDictionary in notificationsToConvert) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:notificationDictionary options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSDictionary *batchDictionary = [NSDictionary dictionaryWithObjectsAndKeys:jsonString,@"batch", nil];
        [parameters addEntriesFromDictionary:batchDictionary];
    }
    
    NSString *path = @"user/del_batch_notification";
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        
    } errorBlock:^(NSError *error) {
        
    }];
}

- (NSArray *)getNotificationsPreparedForJSON:(NSArray *)notificationsList {
//{"type":type, "source":sourceName, "id":elementId}
    NSMutableArray *notificationsForJSON = [NSMutableArray array];
    for (Notification *notification in notificationsList) {
        NSDictionary *notificationsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:notification.type, @"type", notification.sourceName, @"source", notification.elementId, @"id", nil];
        [notificationsForJSON addObject:notificationsDictionary];
        
    }
    return notificationsForJSON.copy;
}

- (void)replyFriendRequest:(BOOL)accept friendId:(NSString *)friendId{
    NSString *acceptString;
    if (accept) {
        acceptString = @"1";
    } else {
        acceptString = @"2";
    }
    NSString *path = @"user/reply_add_friend";
    User *user = self.sessionManager.userLogged;
    NSDictionary *parameters=@{@"uid":user.uid,
                               @"password":user.password,
                               @"fid":friendId,
                               @"accepts":acceptString};
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        
    } errorBlock:^(NSError *error) {
        
    }];

}

@end
