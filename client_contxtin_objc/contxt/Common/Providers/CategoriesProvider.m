//
//  CategoriesProvider.m
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "CategoriesProvider.h"
#import "ContxtCategory.h"
#import "Source.h"
#import "NSManagedObject+JSON.h"

@implementation CategoriesProvider

-(void)categoriesWithSuccessBlock:(RequestManagerSuccess)success errorBlock:(RequestManagerError)errorBlock{
    NSString *path=@"user/get_category_info";
    User *user = [self.sessionManager userLogged];
    NSDictionary *parameters=@{@"uid":user.uid,
                               @"password":user.password};
    
    
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        NSArray *categoriesFromJSON = [data objectForKey:@"data"];
        __block NSInteger categoriesSaved = 0;
        for (NSDictionary *categoryDictionary in categoriesFromJSON) {
            NSString *categoryName = [categoryDictionary valueForKey:@"category"];
            NSArray *sources = [categoryDictionary valueForKey:@"sources"];
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                ContxtCategory *category = [ContxtCategory MR_findFirstByAttribute:@"category" withValue:categoryName inContext:localContext];
                if (!category) {
                    category = [ContxtCategory MR_createEntityInContext:localContext];
                    category.category = categoryName;
                    category.selected = [categoryDictionary valueForKey:@"selected"];
                }
                for (NSDictionary *sourceDictionary in sources) {
                    NSString *name = [sourceDictionary valueForKey:@"name"];
                    NSString *url = [sourceDictionary valueForKey:@"url"];
                    Source *source = [Source MR_findFirstByAttribute:@"name" withValue:name inContext:localContext];
                    if (!source) {
                        source = [Source MR_createEntityInContext:localContext];
                        source.name = name;
                        source.url = url;
                    }
                    source.contxtCategory = category;
                }
                
            } completion:^(BOOL completionSuccess, NSError *error) {
                categoriesSaved += 1;
                ContxtCategory *category = [ContxtCategory MR_findFirstByAttribute:@"category" withValue:categoryName inContext:[NSManagedObjectContext MR_defaultContext]];
                [category addUsersObject:user];
                NSArray *sourcesRetrieved = [Source MR_findByAttribute:@"contxtCategory" withValue:category];
                for (Source *source in sourcesRetrieved) {
                    [category addSourcesObject:source];
                }
                if (categoriesSaved == categoriesFromJSON.count) {
                    success(data);
                }
            }];
        }
    } errorBlock:^(NSError *error) {
        errorBlock(error);
    }];
    
}

- (void)updateCategories:(NSArray *)categories successBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock {
    
    NSString *path = @"user/add_category_info";
    User *user = [self.sessionManager userLogged];
    NSArray *categoriesToConvert = [self getCategoriesPreparedForJSON:categories];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:categoriesToConvert options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *parameters=@{@"uid":user.uid,
                               @"password":user.password,
                               @"categories":jsonString};
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        successBlock(data);
    } errorBlock:^(NSError *error) {
        errorBlock(error);
    }];
}

- (NSArray *)getCategoriesPreparedForJSON:(NSArray *)categories {
    NSMutableArray *categoriesForJSON = [NSMutableArray array];
    for (ContxtCategory *category in categories) {
        NSMutableDictionary *categoriesDictionary = [category toDictionary].mutableCopy;
        NSArray *sources = [category.sources allObjects];
        NSArray *sourcesArray = [self getSourcesPreparedForJSON:sources];
        [categoriesDictionary setObject:sourcesArray forKey:@"sources"];
        [categoriesForJSON addObject:categoriesDictionary];
        
    }
    return categoriesForJSON.copy;
}

- (NSArray *)getSourcesPreparedForJSON:(NSArray *)sources {
    NSMutableArray *sourcesForJSON = [NSMutableArray array];
    for (Source *source in sources) {
        [sourcesForJSON addObject:[source toDictionary]];
    }
    return sourcesForJSON.copy;
}

@end
