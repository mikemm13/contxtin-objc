//
//  NotificationsProvider.h
//  contxt
//
//  Created by Miguel Martin Nieto on 27/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "BaseProvider.h"
#import "SessionManager.h"

@interface NotificationsProvider : BaseProvider
@property (strong, nonatomic) SessionManager *sessionManager;

-(void)requestNotifications;
-(void)clearAllNotifications:(NSArray *)notificationsList;
-(void)replyFriendRequest:(BOOL)accept friendId:(NSString *)friendId;

@end
