//
//  CloudsProvider.m
//  contxt
//
//  Created by Miguel Martin Nieto on 22/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "CloudsProvider.h"
#import "ContxtCategory.h"
#import "Cloud.h"
#import "Tag.h"
#import "Ranking.h"
#import "Dialogue.h"
#import "Comment.h"

@implementation CloudsProvider


-(void)cloudsWithSuccessBlock:(RequestManagerSuccess)success errorBlock:(RequestManagerError)errorBlock{
    NSString *path=@"user/magic";
    User *user = [self.sessionManager userLogged];
    NSDictionary *parameters=@{@"uid":user.uid,
                               @"password":user.password,
                               @"viewparam":@"default",
                               @"category":@"All"};
    
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
            if ([[data valueForKey:@"status"] isEqualToString:@"ok"]) {
                [self createSocialClouds:data];
                [self createClouds:data];
                [self savedCloudsWithSuccessBlock:^(id successData) {
                        success(data);
                } errorBlock:^(NSError *error) {
                    
                }];

            }
    } errorBlock:^(NSError *error) {
        errorBlock(error);
    }];
    
}

- (void)savedCloudsWithSuccessBlock:(RequestManagerSuccess)success errorBlock:(RequestManagerError)errorBlock {
    NSString *path = @"user/saved_magic";
    User *user = [self.sessionManager userLogged];
    NSDictionary *parameters = @{@"uid":user.uid,
                                 @"password":user.password};
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        if ([[data valueForKey:@"status"] isEqualToString:@"ok"]) {
            
            NSDictionary *allClouds = [[data objectForKey:@"data"] objectForKey:@"saved_data"];
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                [self buildCloudsInContext:localContext saved:YES category:nil allCloudsToBuild:allClouds];
            } completion:^(BOOL contextDidSave, NSError *error) {
                if (contextDidSave) {
                    success(data);                    
                }
            }];
        }
    } errorBlock:^(NSError *error) {
        errorBlock(error);
    }];
}


- (void)createClouds:(id)data {
    NSArray *cached_data = [[data objectForKey:@"data"] valueForKey:@"cached_data"];
    if (![cached_data isEqual:[NSNull null]]) {
        NSDictionary *cloudsElements = [[[data objectForKey:@"data"] objectForKey:@"cached_data"] objectForKey:@"magicElements"];
        
        for (NSDictionary *categoryData in cloudsElements) {
            NSString *categoryName = [NSString stringWithFormat:@"%@", categoryData];
            NSDictionary *allCloudsInCategory = [[cloudsElements objectForKey:categoryName] objectForKey:@"clouds"];
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                ContxtCategory *category = [ContxtCategory MR_findFirstByAttribute:@"category" withValue:categoryName inContext:localContext];
                if (!category) {
                    category = [ContxtCategory MR_createEntityInContext:localContext];
                    category.category = categoryName;
                }
                [self buildCloudsInContext:localContext saved:NO category:category allCloudsToBuild:allCloudsInCategory];
            } completion:^(BOOL contextDidSave, NSError *error) {
                //            [self createSocialClouds:data];
            }];
        }
    }
}

- (void)buildCloudsInContext:(NSManagedObjectContext *)localContext saved:(BOOL)saved category:(ContxtCategory *)category allCloudsToBuild:(NSDictionary *)allCloudsInCategory {
    if (![allCloudsInCategory isEqual:@"no-data"]) {
        
        for (NSDictionary *cloudDictionary in allCloudsInCategory) {
            NSString *cloudId = [cloudDictionary valueForKey:@"cid"];
            Cloud *cloud = [Cloud MR_findFirstByAttribute:@"cid" withValue:cloudId inContext:localContext];
            if (!cloud) {
                cloud = [Cloud MR_createEntityInContext:localContext];
                cloud.cid = cloudId;
            }
            cloud.contxtCategory = category;
            if (saved) {
                cloud.saved = @1;
            } else {
                cloud.saved = @0;
            }
            NSArray *tagsFromJSON = [cloudDictionary objectForKey:@"tags"];
            for (int i = 0; i < tagsFromJSON.count; i++) {
                Tag *tag = [Tag MR_createEntityInContext:localContext];
                tag.name = [tagsFromJSON objectAtIndex:i];
                tag.cloud = cloud;
            }
            
            for (NSDictionary *rankingElement in [cloudDictionary objectForKey:@"ranking"]) {
                NSString *link = rankingElement[@"link"];
                NSString *cloudCid = cloud.cid;
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"link = %@ AND cloud.cid = %@", link,cloudCid];
                Ranking *ranking = [Ranking MR_findFirstWithPredicate:predicate inContext:localContext];
                if (!ranking) {
                    ranking = [Ranking MR_createEntityInContext:localContext];
                }
                ranking.body = [rankingElement valueForKey:@"clean_body"];
                if ([[rankingElement valueForKey:@"clean_image"] isKindOfClass:[NSArray class]] || [[rankingElement valueForKey:@"clean_image"] isKindOfClass:[NSNull class] ]) {
                    ranking.image = @"";
                } else {
                    ranking.image = [rankingElement valueForKey:@"clean_image"];
                }
                ranking.title = [rankingElement valueForKey:@"title"];
                ranking.link = [rankingElement valueForKey:@"link"];
                ranking.sourceName = [rankingElement valueForKey:@"name"];
                
                NSString *dateString = [rankingElement valueForKey:@"pub_date"];
                if (![dateString isKindOfClass:[NSNull class]]) {
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                    NSDate *dateFromString = [[NSDate alloc] init];
                    dateFromString = [dateFormatter dateFromString:dateString];
                    
                    ranking.publishDate = dateFromString;
                }
                float time = [[rankingElement valueForKey:@"read_time"] floatValue] * 60;
                int seconds = fmodf(time, 60.0);
                int minutes = (time - seconds) / 60;
                int minutesToShow = minutes;
                int secondsToShow = 0;
                if (minutes < 1) {
                    minutesToShow = 1;
                    secondsToShow = 0;
                } else if (seconds > 0 && seconds <= 30) {
                    secondsToShow = 30;
                }
                ranking.readingTime = [NSString stringWithFormat:@"%.2d min %.2d sec", minutesToShow, secondsToShow];
                ranking.cloud = cloud;
            }
        }
    }
}


- (void)createSocialClouds:(id)data {
    NSDictionary *dataDictionary = [data objectForKey:@"data"];
    if (dataDictionary) {
        NSDictionary *cloudsElements = [dataDictionary objectForKey:@"social_data"];
        if (![cloudsElements isEqual:@"no-data"]) {
            __block NSString *cloudId;
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                for (NSDictionary *socialCloud in cloudsElements) {
                    ContxtCategory *category = [ContxtCategory MR_findFirstByAttribute:@"category" withValue:@"Conversations" inContext:localContext];
                    if (!category) {
                        category = [ContxtCategory MR_createEntityInContext:localContext];
                        category.category = @"Conversations";
                    }
//                    [self buildSocialCloudsInContext:localContext category:category cloudsToBuild:
                    NSString *dialogueId = [socialCloud valueForKey:@"did"];
                    
                    Dialogue *dialogue = [Dialogue MR_findFirstByAttribute:@"dialogueId" withValue:dialogueId inContext:localContext];
                    if (!dialogue) {
                        dialogue = [Dialogue MR_createEntityInContext:localContext];
                        dialogue.dialogueId = dialogueId;
                    }
                    cloudId = [NSString stringWithFormat:@"%@",dialogueId];
                    Cloud *cloud = [Cloud MR_findFirstByAttribute:@"cid" withValue:cloudId inContext:localContext];
                    if (!cloud) {
                        cloud = [Cloud MR_createEntityInContext:localContext];
                        cloud.cid = cloudId;
                    }
                    [category addCloudsObject:cloud];
                    NSArray *tagsFromJSON = [socialCloud objectForKey:@"tags"];
                    for (int i = 0; i < tagsFromJSON.count; i++) {
                        Tag *tag = [Tag MR_createEntityInContext:localContext];
                        tag.name = [tagsFromJSON objectAtIndex:i];
                        tag.cloud = cloud;
                    }
                    
                    for (NSDictionary *rankingElement in [socialCloud objectForKey:@"ranking"]) {
                        
                        NSString *link = rankingElement[@"link"];
                        NSString *cloudCid = cloud.cid;
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"link = %@ AND cloud.cid = %@", link,cloudCid];
                        Ranking *ranking = [Ranking MR_findFirstWithPredicate:predicate inContext:localContext];
                        if (!ranking) {
                            ranking = [Ranking MR_createEntityInContext:localContext];
                        }
                        ranking.body = [rankingElement valueForKey:@"clean_body"];
                        if ([[rankingElement valueForKey:@"clean_image"] isKindOfClass:[NSArray class]] || [[rankingElement valueForKey:@"clean_image"] isKindOfClass:[NSNull class] ]) {
                            ranking.image = @"";
                        } else {
                            ranking.image = [rankingElement valueForKey:@"clean_image"];
                        }
                        ranking.title = [rankingElement valueForKey:@"title"];
                        ranking.link = [rankingElement valueForKey:@"link"];
                        ranking.sourceName = [rankingElement valueForKey:@"name"];
                            
                        NSString *dateString = [rankingElement valueForKey:@"pub_date"];
                        if (![dateString isKindOfClass:[NSNull class]]) {
                            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                            NSDate *dateFromString = [[NSDate alloc] init];
                            dateFromString = [dateFormatter dateFromString:dateString];
                                
                            ranking.publishDate = dateFromString;
                        }
                        float time = [[rankingElement valueForKey:@"read_time"] floatValue] * 60;
                        int seconds = fmodf(time, 60.0);
                        int minutes = (time - seconds) / 60;
                        int minutesToShow = minutes;
                        int secondsToShow = 0;
                        if (minutes < 1) {
                            minutesToShow = 1;
                            secondsToShow = 0;
                        } else if (seconds > 0 && seconds <= 30) {
                            secondsToShow = 30;
                        }
                        ranking.readingTime = [NSString stringWithFormat:@"%.2d min %.2d sec", minutesToShow, secondsToShow];
                        ranking.cloud = cloud;
                        
                    }
                    
                    NSDictionary *commentsDictionary = [socialCloud objectForKey:@"comments"];
                    for (NSDictionary *commentElement in commentsDictionary) {
                        NSString *commentDateStr = [commentElement objectForKey:@"date"];
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
                        NSDate *commentDate = [[NSDate alloc] init];
                        commentDate = [dateFormatter dateFromString:commentDateStr];
                        if ([[commentElement valueForKey:@"uid"] isKindOfClass:[NSNumber class]]) {
                            //                        comment.uid =  [[commentElement valueForKey:@"uid"] stringValue];
                        } else {
                            Comment *comment = [Comment MR_findFirstByAttribute:@"date" withValue:commentDate inContext:localContext];
                            if (!comment) {
                                Comment *comment = [Comment MR_createEntityInContext:localContext];
                                comment.uid = [commentElement valueForKey:@"uid"];
                                comment.name = [commentElement valueForKey:@"name"];
                                comment.message = [commentElement valueForKey:@"comment"];
                                comment.date = commentDate;
                                [dialogue addCommentsObject:comment];
                            }
                        }
                    }
                        
                    dialogue.cloud = cloud;
                
                }
            } completion:^(BOOL contextDidSave, NSError *error) {

            }];
        }
        
    }
}

- (void)saveCloudWithId:(NSString *)cid successBlock:(RequestManagerSuccess)saveSuccessBlock errorBlock:(RequestManagerError)saveErrorBlock {
    NSString *path=@"user/save_cloud";
    User *user = [self.sessionManager userLogged];
    NSDictionary *parameters=@{@"uid":user.uid,
                               @"password":user.password,
                               @"cid":cid
                               };
    
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        if ([[data valueForKey:@"status"] isEqualToString:@"ok"]) {
            saveSuccessBlock(data);
        }
    } errorBlock:^(NSError *error) {
        saveErrorBlock(error);
    }];

}

- (void)unsaveCloudWithId:(NSString *)cid successBlock:(RequestManagerSuccess)unsaveSuccessBlock errorBlock:(RequestManagerError)unsaveErrorBlock {
    NSString *path=@"user/discard_saved_cloud";
    User *user = [self.sessionManager userLogged];
    NSDictionary *parameters=@{@"uid":user.uid,
                               @"password":user.password,
                               @"cid":cid
                               };
    
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        if ([[data valueForKey:@"status"] isEqualToString:@"ok"]) {
            unsaveSuccessBlock(data);
        }
    } errorBlock:^(NSError *error) {
        unsaveErrorBlock(error);
    }];
}

- (void)discardCloudWithDialogueId:(NSString *)dialogueId successBlock:(RequestManagerSuccess)discardSuccessBlock errorBlock:(RequestManagerError)unsaveErrorBlock {
    NSString *path=@"user/discard_shared_cloud";
    User *user = [self.sessionManager userLogged];
    NSDictionary *parameters=@{@"uid":user.uid,
                               @"password":user.password,
                               @"did":dialogueId
                               };
    
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        if ([[data valueForKey:@"status"] isEqualToString:@"ok"]) {
            discardSuccessBlock(data);
        }
    } errorBlock:^(NSError *error) {
        unsaveErrorBlock(error);
    }];
}

- (void)commentCloudWithId:(NSString *)cid toFriends:(NSArray *)friends message:(NSString *)message successBlock:(RequestManagerSuccess)commentSuccessBlock errorBlock:(RequestManagerError)commentErrorBlock {
    NSString *path = @"user/promote";
    User *user = [self.sessionManager userLogged];
    NSMutableDictionary *parameters=[NSMutableDictionary dictionaryWithObjectsAndKeys:user.uid, @"uid", user.password, @"password", cid, @"cid", message, @"comment", nil];
    for (NSString *friendId in friends) {
        NSDictionary *friendsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:friendId, @"to_whom", nil];
        [parameters addEntriesFromDictionary:friendsDictionary];
    }
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        if ([[data valueForKey:@"status"] isEqualToString:@"ok"]) {
            commentSuccessBlock(data);
        }
    } errorBlock:^(NSError *error) {
        commentErrorBlock(error);
    }];
}

- (void)addCommentToDialogue:(NSString *)dialogueId participants:(NSArray *)participants comment:(NSString *)comment successBlock:(RequestManagerSuccess)commentSuccessBlock errorBlock:(RequestManagerError)commentErrorBlock {
    NSString *path = @"user/comment";
    User *user = [self.sessionManager userLogged];
    NSMutableDictionary *parameters=[NSMutableDictionary dictionaryWithObjectsAndKeys:user.uid, @"uid", user.password, @"password", dialogueId, @"did", comment, @"comment", nil];
    for (NSString *friendId in participants) {
        NSDictionary *friendsDictionary = [NSDictionary dictionaryWithObjectsAndKeys:friendId, @"to_whom", nil];
        [parameters addEntriesFromDictionary:friendsDictionary];
    }
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        if ([[data valueForKey:@"status"] isEqualToString:@"ok"]) {
            commentSuccessBlock(data);
        }
    } errorBlock:^(NSError *error) {
        commentErrorBlock(error);
    }];

}

@end
