//
//  FriendsProvider.h
//  contxt
//
//  Created by Miguel Martin Nieto on 20/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "BaseProvider.h"
#import "SessionManager.h"

@interface FriendsProvider : BaseProvider
@property (strong,nonatomic) SessionManager *sessionManager;
- (void)friendsWithSuccessBlock:(RequestManagerSuccess)success errorBlock:(RequestManagerError)errorBlock;
- (void)updateFriends:(NSArray *)friends successBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock;
- (void)searchFriendsWithName:(NSString *)name completion:(RequestSearchSuccess)resultsBlock;
- (void)addFriendWithId:(NSString *)friendId successBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock;
- (void)removeFriendWithId:(NSString *)friendId successBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock;
@end
