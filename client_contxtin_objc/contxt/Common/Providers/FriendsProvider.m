//
//  FriendsProvider.m
//  contxt
//
//  Created by Miguel Martin Nieto on 20/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "FriendsProvider.h"
#import "Friend.h"

@implementation FriendsProvider

-(void)friendsWithSuccessBlock:(RequestManagerSuccess)success errorBlock:(RequestManagerError)errorBlock {
    NSString *path = @"user/get_friends";
    User *user = [self.sessionManager userLogged];
    NSDictionary *parameters=@{@"uid":user.uid,
                               @"password":user.password};
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        NSArray *friendsFromJSON = [data objectForKey:@"data"];
        __block NSInteger friendsSaved = 0;
        for (NSDictionary *friendDictionary in friendsFromJSON) {
            NSString *friendUid = [friendDictionary valueForKey:@"uid"];
            NSString *friendName = [friendDictionary valueForKey:@"name"];
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                Friend *friend = [Friend MR_findFirstByAttribute:@"uid" withValue:friendUid inContext:localContext];
                if (!friend) {
                    friend = [Friend MR_createEntityInContext:localContext];
                    friend.uid = friendUid;
                    friend.username = friendName;
                }
                [user addFriendsObject:friend];
            } completion:^(BOOL contextDidSave, NSError *error) {
                friendsSaved += 1;
                Friend *friend = [Friend MR_findFirstByAttribute:@"uid" withValue:friendUid inContext:[NSManagedObjectContext MR_defaultContext]];
                [friend addFriendOfUserObject:user];
                if (friendsSaved == friendsFromJSON.count) {
                    success(data);
                }
            }];
        }
    } errorBlock:^(NSError *error) {
        
    }];
}

- (void)updateFriends:(NSArray *)friends successBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock {
    
}

- (void)searchFriendsWithName:(NSString *)name completion:(RequestSearchSuccess)resultsBlock {
    NSString *path = @"user/search_friend";
    User *user = [self.sessionManager userLogged];
    NSDictionary *parameters=@{@"uid":user.uid,
                               @"password":user.password,
                               @"friend_name":name};
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        if ([[data valueForKey:@"status"] isEqualToString:@"ok"]) {
            NSArray *friendsFromJSON = [data objectForKey:@"data"];
            NSMutableArray *friendsList = [NSMutableArray array];
            for (NSDictionary *friendDictionary in friendsFromJSON) {
                NSString *friendUid = [friendDictionary valueForKey:@"uid"];
                NSString *friendName = [friendDictionary valueForKey:@"name"];
                Friend *friend = [Friend MR_findFirstByAttribute:@"uid" withValue:friendUid inContext:[NSManagedObjectContext MR_defaultContext]];
                if (!friend) {
                    friend = [Friend MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
                    friend.uid = friendUid;
                    friend.username = friendName;
                    friend.invited = NO;
                }
                [friendsList addObject:friend];
            }
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
                resultsBlock(friendsList.copy);
            }];
        }
    } errorBlock:^(NSError *error) {
        
    }];
}

- (void)addFriendWithId:(NSString *)friendId successBlock:(RequestManagerSuccess)success errorBlock:(RequestManagerError)errorBlock {
    NSString *path = @"user/request_add_friend";
    User *user = [self.sessionManager userLogged];
    NSDictionary *parameters=@{@"uid":user.uid,
                               @"password":user.password,
                               @"fid":friendId};
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        if ([[data valueForKey:@"status"] isEqualToString:@"ok"]) {
            Friend *invitedFriend = [Friend MR_findFirstByAttribute:@"uid" withValue:friendId inContext:[NSManagedObjectContext MR_defaultContext]];
            invitedFriend.invited = YES;
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
                success(nil);
                
            }];
        } else {
            success([data objectForKey:@"error"]);
        }
    } errorBlock:^(NSError *error) {
        
    }];
}

- (void)removeFriendWithId:(NSString *)friendId successBlock:(RequestManagerSuccess)success errorBlock:(RequestManagerError)errorBlock {
    NSString *path = @"user/del_friend";
    User *user = [self.sessionManager userLogged];
    NSDictionary *parameters=@{@"uid":user.uid,
                               @"password":user.password,
                               @"fid":friendId};
    [self.requestManager POST:path parameters:parameters successBlock:^(id data) {
        if ([[data valueForKey:@"status"] isEqualToString:@"ok"]) {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                Friend *friend = [Friend MR_findFirstByAttribute:@"uid" withValue:friendId inContext:localContext];
                [friend MR_deleteEntityInContext:localContext];
                [user removeFriendsObject:friend];
            } completion:^(BOOL contextDidSave, NSError *error) {
                success(data);
            }];
        }
        
    } errorBlock:^(NSError *error) {
        
    }];
}

@end
