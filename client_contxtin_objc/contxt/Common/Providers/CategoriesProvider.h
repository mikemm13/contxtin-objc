//
//  CategoriesProvider.h
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "BaseProvider.h"
#import "SessionManager.h"

@interface CategoriesProvider : BaseProvider
@property (strong,nonatomic) SessionManager *sessionManager;
- (void)categoriesWithSuccessBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock;
- (void)updateCategories:(NSArray *)categories successBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock;
@end
