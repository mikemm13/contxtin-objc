//
//  DashboardInteractor.m
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "CategoriesInteractor.h"
#import "CategoriesProvider.h"

@interface CategoriesInteractor()
@property (strong, nonatomic)CategoriesProvider *categoriesProvider;
@end

@implementation CategoriesInteractor
- (instancetype)initWithSessionManager:(SessionManager *)sessionManager
{
    self = [super init];
    if (self) {
        _categoriesProvider = [[CategoriesProvider alloc] init];
        _categoriesProvider.sessionManager = sessionManager;
    }
    return self;
}

- (void)categoriesItemsWithCompletion:(void(^)(void))completion{
    [self.categoriesProvider categoriesWithSuccessBlock:^(id data) {
        completion();
    } errorBlock:^(NSError *error) {
        //handle error
    }];
}

- (void)updateCategories:(NSArray *)categories completion:(void (^)(void))completion {
    [self.categoriesProvider updateCategories:categories successBlock:^(id data){
        completion();
    } errorBlock:^(NSError *error) {
        //error
    }];
}

@end
