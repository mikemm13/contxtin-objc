//
//  CloudsInteractor.h
//  contxt
//
//  Created by Miguel Martin Nieto on 22/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"

@interface CloudsInteractor : NSObject
- (instancetype)initWithSessionManager:(SessionManager *)sessionManager;
- (void)cloudsWithCompletion:(void(^)(void))completion;
-(void)saveCloudWithId:(NSString *)cid completion:(void(^)(BOOL logoutSucceeded, NSString *message))completion;
-(void)unsaveCloudWithId:(NSString *)cid completion:(void(^)(BOOL logoutSucceeded, NSString *message))completion;
-(void)addCommentToDialogue:(NSString *)dialogueId participants:(NSArray *)participants comment:(NSString *)comment completion:(void(^)(BOOL commentSuccess, NSString *message))completion;
- (void)discardCloudWithDialogueId:(NSString *)dialogueId completion:(void (^)(BOOL, NSString *))completion;
@end
