//
//  DashboardInteractor.h
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"

@interface CategoriesInteractor : NSObject

- (instancetype)initWithSessionManager:(SessionManager *)sessionManager;
- (void)categoriesItemsWithCompletion:(void(^)(void))completion;
- (void)updateCategories:(NSArray *)categories completion:(void(^)(void))completion;

@end
