//
//  FriendsInteractor.h
//  contxt
//
//  Created by Miguel Martin Nieto on 20/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionManager.h"

@interface FriendsInteractor : NSObject

- (instancetype)initWithSessionManager:(SessionManager *)sessionManager;
- (void)friendsItemsWithCompletion:(void(^)(void))completion;
- (void)updateFriends:(NSArray *)friends completion:(void(^)(void))completion;
- (NSArray *)searchFriendsWithName:(NSString *)name completion:(void(^)(NSArray *fetchedResults))completion;
- (void)addFriendWithId:(NSString *)friendId completion:(void(^)(void))completion error:(void (^)(NSString *errorCode))error;
- (void)removeFriendWithId:(NSString *)friendId completion:(void(^)(void))completion;
@end
