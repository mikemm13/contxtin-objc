//
//  FriendsInteractor.m
//  contxt
//
//  Created by Miguel Martin Nieto on 20/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "FriendsInteractor.h"
#import "FriendsProvider.h"

@interface FriendsInteractor()
@property (strong, nonatomic)FriendsProvider *friendsProvider;
@end

@implementation FriendsInteractor

- (instancetype)initWithSessionManager:(SessionManager *)sessionManager {
    self = [super init];
    if (self) {
        _friendsProvider = [[FriendsProvider alloc] init];
        _friendsProvider.sessionManager = sessionManager;
    }
    return self;
}

- (void)friendsItemsWithCompletion:(void(^)(void))completion {
    [self.friendsProvider friendsWithSuccessBlock:^(id data) {
        completion();
    } errorBlock:^(NSError *error) {
        
    }];
}

- (void)updateFriends:(NSArray *)friends completion:(void(^)(void))completion {
    [self.friendsProvider updateFriends:friends successBlock:^(id data) {
        completion();
    } errorBlock:^(NSError *error) {
        
    }];
}

- (NSArray *)searchFriendsWithName:(NSString *)name completion:(void(^)(NSArray *fetchedResults))completion{
    [self.friendsProvider searchFriendsWithName:name completion:^(NSArray *results) {
        completion(results);
    }];
    return nil;
}

- (void)addFriendWithId:(NSString *)friendId completion:(void (^)(void))completion error:(void (^)(NSString *errorCode))error {
    [self.friendsProvider addFriendWithId:friendId successBlock:^(id data) {
        if (data) {
            error([data valueForKey:@"code"]);
        } else {
            completion();
        }
    } errorBlock:^(NSError *error) {
        
    }];
}

- (void)removeFriendWithId:(NSString *)friendId completion:(void (^)(void))completion {
    [self.friendsProvider removeFriendWithId:friendId successBlock:^(id data) {
        completion();
    } errorBlock:^(NSError *error) {
        
    }];
}

@end
