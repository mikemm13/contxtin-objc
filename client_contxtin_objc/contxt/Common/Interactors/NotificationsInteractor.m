//
//  NotificationsInteractor.m
//  contxt
//
//  Created by Miguel Martin Nieto on 27/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "NotificationsInteractor.h"
#import "NotificationsProvider.h"

@interface NotificationsInteractor ()

@property (strong, nonatomic) NotificationsProvider *notificationsProvider;

@end

@implementation NotificationsInteractor

- (instancetype)initWithSessionManager:(SessionManager *)sessionManager {
    self = [super init];
    if (self) {
        _notificationsProvider = [[NotificationsProvider alloc] init];
        _notificationsProvider.sessionManager = sessionManager;
    }
    return self;
}

- (void)requestNotifications {
    [self.notificationsProvider requestNotifications];
}

- (void)clearAllNotifications:(NSArray *)notificationsList {
    [self.notificationsProvider clearAllNotifications:notificationsList];
}

- (void)replyFriendRequest:(BOOL)accept friendId:(NSString *)friendId{
    [self.notificationsProvider replyFriendRequest:accept friendId:friendId];
}

@end
