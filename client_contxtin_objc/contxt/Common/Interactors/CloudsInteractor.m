//
//  CloudsInteractor.m
//  contxt
//
//  Created by Miguel Martin Nieto on 22/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "CloudsInteractor.h"
#import "CloudsProvider.h"

@interface CloudsInteractor()
@property (strong, nonatomic)CloudsProvider *cloudsProvider;
@end

@implementation CloudsInteractor

- (instancetype)initWithSessionManager:(SessionManager *)sessionManager
{
    self = [super init];
    if (self) {
        _cloudsProvider = [[CloudsProvider alloc] init];
        _cloudsProvider.sessionManager = sessionManager;
    }
    return self;
}

- (void)cloudsWithCompletion:(void(^)(void))completion{
    [self.cloudsProvider cloudsWithSuccessBlock:^(id data) {
        completion();
    } errorBlock:^(NSError *error) {
        //handle error
    }];
}

- (void)saveCloudWithId:(NSString *)cid completion:(void (^)(BOOL, NSString *))completion {
    [self.cloudsProvider saveCloudWithId:cid successBlock:^(id data) {
        completion(YES,nil);
    } errorBlock:^(NSError *error) {
        
    }];
}

- (void)unsaveCloudWithId:(NSString *)cid completion:(void (^)(BOOL, NSString *))completion {
    [self.cloudsProvider unsaveCloudWithId:cid successBlock:^(id data) {
        completion(YES,nil);
    } errorBlock:^(NSError *error) {
        
    }];
}

- (void)discardCloudWithDialogueId:(NSString *)dialogueId completion:(void (^)(BOOL, NSString *))completion {
    [self.cloudsProvider discardCloudWithDialogueId:dialogueId successBlock:^(id data) {
        completion(YES, nil);
    } errorBlock:^(NSError *error) {
        
    }];
}

- (void)commentCloudWithId:(NSString *)cid toFriends:(NSArray *)friends message:(NSString *)message completion:(void (^)(BOOL, NSString *))completion {
    [self.cloudsProvider commentCloudWithId:cid toFriends:friends message:message successBlock:^(id data) {
        
    } errorBlock:^(NSError *error) {
        
    }];
}

- (void)addCommentToDialogue:(NSString *)dialogueId participants:(NSArray *)participants comment:(NSString *)comment completion:(void (^)(BOOL, NSString *))completion {
    [self.cloudsProvider addCommentToDialogue:dialogueId participants:participants comment:comment successBlock:^(id data){
        completion (YES, nil);
    } errorBlock:^(NSError *error) {
        
    }];
}

@end
