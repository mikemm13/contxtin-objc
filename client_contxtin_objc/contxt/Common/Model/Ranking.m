//
//  Ranking.m
//  
//
//  Created by Miguel Martin Nieto on 14/8/15.
//
//

#import "Ranking.h"
#import "Cloud.h"


@implementation Ranking

@dynamic body;
@dynamic image;
@dynamic link;
@dynamic publishDate;
@dynamic readingTime;
@dynamic sourceName;
@dynamic title;
@dynamic cloud;

@end
