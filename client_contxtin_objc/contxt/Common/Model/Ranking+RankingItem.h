//
//  Ranking+RankingItem.h
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "Ranking.h"
#import "RankingItemProtocol.h"

@interface Ranking (RankingItem)<RankingItemProtocol>

@end
