//
//  Source.h
//  
//
//  Created by Miguel Martin Nieto on 14/8/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ContxtCategory;

@interface Source : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) ContxtCategory *contxtCategory;

@end
