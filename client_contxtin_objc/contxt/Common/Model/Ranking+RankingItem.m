//
//  Ranking+RankingItem.m
//  contxt
//
//  Created by Miguel Martin Nieto on 23/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "Ranking+RankingItem.h"
#import "CloudRankingCellDrawer.h"
#import "RankingDetailViewController.h"
#import "CloudsWireFrame.h"
@implementation Ranking (RankingItem)

- (NSString *)itemTitle{
    return self.title;
}

- (id<RankingCellDrawerProtocol>)cellDrawer{
    return [[CloudRankingCellDrawer alloc] init];
}
- (UIViewController *)contentsViewControllerWithSessionManager:(SessionManager *)sessionManager router:(CloudsWireFrame *)router{
    return [[RankingDetailViewController alloc] initWithNibName:@"RankingDetailViewController" ranking:self sessionManager:sessionManager router:router];
    
}

@end
