//
//  Dialogue.m
//  
//
//  Created by Miguel Martin Nieto on 14/8/15.
//
//

#import "Dialogue.h"
#import "Cloud.h"
#import "Comment.h"


@implementation Dialogue

@dynamic dialogueId;
@dynamic cloud;
@dynamic comments;

@end
