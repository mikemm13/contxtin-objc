//
//  Cloud.m
//  
//
//  Created by Miguel Martin Nieto on 15/9/15.
//
//

#import "Cloud.h"
#import "ContxtCategory.h"
#import "Dialogue.h"
#import "Ranking.h"
#import "Tag.h"


@implementation Cloud

@dynamic cid;
@dynamic saved;
@dynamic contxtCategory;
@dynamic dialogue;
@dynamic rankingElements;
@dynamic tags;

@end
