//
//  ContxtCategory+DashboardItem.m
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "ContxtCategory+DashboardItem.h"
#import "DashboardCategoryCellDrawer.h"
#import "CategoryCellDrawer.h"
#import "CloudsRootViewController.h"
#import "CategoryCellDrawerProtocol.h"
#import "CellDrawerProtocol.h"
#import <objc/runtime.h>

static const void *screenKey = &screenKey;

@implementation ContxtCategory (DashboardItem)

- (NSString *)itemTitle{
    return self.category;
}

- (id<CategoryCellDrawerProtocol, CellDrawerProtocol>)cellDrawer{
    if ([self.screenWhereCategoryIsShown isEqualToString:@"Dashboard"]) {
        return [[DashboardCategoryCellDrawer alloc] init];
    } else if ([self.screenWhereCategoryIsShown isEqualToString:@"Categories"]) {
        return [[CategoryCellDrawer alloc] init];
    }
    return nil;
}
- (UIViewController *)contentsViewControllerWithSessionManager:(SessionManager *)sessionManager{
    if ([self.screenWhereCategoryIsShown isEqualToString:@"Dashboard"]) {
        return [[CloudsRootViewController alloc] initWithNibName:@"CloudsRootViewController" categoryName:self.category sessionManager:sessionManager isSavedScreen:NO];
    }
    return nil;
}

- (NSString *)screenWhereCategoryIsShown {
    return objc_getAssociatedObject(self, screenKey);
}

- (void)setScreenWhereCategoryIsShown:(NSString *)screenWhereCategoryIsShown{
    objc_setAssociatedObject(self, screenKey, screenWhereCategoryIsShown, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
