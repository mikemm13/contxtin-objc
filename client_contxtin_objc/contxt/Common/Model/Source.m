//
//  Source.m
//  
//
//  Created by Miguel Martin Nieto on 14/8/15.
//
//

#import "Source.h"
#import "ContxtCategory.h"


@implementation Source

@dynamic name;
@dynamic url;
@dynamic contxtCategory;

@end
