//
//  Friend.m
//  
//
//  Created by Miguel Martin Nieto on 20/9/15.
//
//

#import "Friend.h"
#import "User.h"


@implementation Friend

@dynamic uid;
@dynamic username;
@dynamic invited;
@dynamic friendOfUser;

@end
