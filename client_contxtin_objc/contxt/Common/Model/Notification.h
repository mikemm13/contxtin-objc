//
//  Notification.h
//  
//
//  Created by Miguel Martin Nieto on 27/9/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Notification : NSManagedObject

@property (nonatomic, retain) NSString * elementId;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSString * sourceName;
@property (nonatomic, retain) NSString * sourceId;
@property (nonatomic, retain) User *user;

@end
