//
//  Tag.m
//  
//
//  Created by Miguel Martin Nieto on 14/8/15.
//
//

#import "Tag.h"
#import "Cloud.h"


@implementation Tag

@dynamic name;
@dynamic cloud;

@end
