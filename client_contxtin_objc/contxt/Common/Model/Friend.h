//
//  Friend.h
//  
//
//  Created by Miguel Martin Nieto on 20/9/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Friend : NSManagedObject

@property (nonatomic, retain) NSString * uid;
@property (nonatomic, retain) NSString * username;
@property (nonatomic) BOOL invited;
@property (nonatomic, retain) NSSet *friendOfUser;
@end

@interface Friend (CoreDataGeneratedAccessors)

- (void)addFriendOfUserObject:(User *)value;
- (void)removeFriendOfUserObject:(User *)value;
- (void)addFriendOfUser:(NSSet *)values;
- (void)removeFriendOfUser:(NSSet *)values;

@end
