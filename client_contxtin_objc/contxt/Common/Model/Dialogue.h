//
//  Dialogue.h
//  
//
//  Created by Miguel Martin Nieto on 14/8/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cloud, Comment;

@interface Dialogue : NSManagedObject

@property (nonatomic, retain) NSString * dialogueId;
@property (nonatomic, retain) Cloud *cloud;
@property (nonatomic, retain) NSSet *comments;
@end

@interface Dialogue (CoreDataGeneratedAccessors)

- (void)addCommentsObject:(Comment *)value;
- (void)removeCommentsObject:(Comment *)value;
- (void)addComments:(NSSet *)values;
- (void)removeComments:(NSSet *)values;

@end
