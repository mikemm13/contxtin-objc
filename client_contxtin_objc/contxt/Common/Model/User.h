//
//  User.h
//  
//
//  Created by Miguel Martin Nieto on 26/9/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ContxtCategory, Friend, Notification;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * uid;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSSet *contxtCategories;
@property (nonatomic, retain) NSSet *friends;
@property (nonatomic, retain) NSSet *notifications;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addContxtCategoriesObject:(ContxtCategory *)value;
- (void)removeContxtCategoriesObject:(ContxtCategory *)value;
- (void)addContxtCategories:(NSSet *)values;
- (void)removeContxtCategories:(NSSet *)values;

- (void)addFriendsObject:(Friend *)value;
- (void)removeFriendsObject:(Friend *)value;
- (void)addFriends:(NSSet *)values;
- (void)removeFriends:(NSSet *)values;

- (void)addNotificationsObject:(Notification *)value;
- (void)removeNotificationsObject:(Notification *)value;
- (void)addNotifications:(NSSet *)values;
- (void)removeNotifications:(NSSet *)values;

@end
