//
//  User.m
//  
//
//  Created by Miguel Martin Nieto on 26/9/15.
//
//

#import "User.h"
#import "ContxtCategory.h"
#import "Friend.h"
#import "Notification.h"


@implementation User

@dynamic email;
@dynamic password;
@dynamic uid;
@dynamic username;
@dynamic contxtCategories;
@dynamic friends;
@dynamic notifications;

@end
