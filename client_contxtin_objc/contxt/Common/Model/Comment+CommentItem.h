//
//  Comment+CommentItem.h
//  contxt
//
//  Created by Miguel Martin Nieto on 25/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "Comment.h"
#import "SocialRankingItemProtocol.h"

@interface Comment (CommentItem)<SocialRankingItemProtocol>

@end
