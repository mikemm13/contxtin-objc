//
//  ContxtCategory.m
//  
//
//  Created by Miguel Martin Nieto on 2/9/15.
//
//

#import "ContxtCategory.h"
#import "Cloud.h"
#import "Source.h"
#import "User.h"


@implementation ContxtCategory

@dynamic category;
@dynamic selected;
@dynamic clouds;
@dynamic sources;
@dynamic users;

@end
