//
//  Ranking.h
//  
//
//  Created by Miguel Martin Nieto on 14/8/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cloud;

@interface Ranking : NSManagedObject

@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSDate * publishDate;
@property (nonatomic, retain) NSString * readingTime;
@property (nonatomic, retain) NSString * sourceName;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Cloud *cloud;

@end
