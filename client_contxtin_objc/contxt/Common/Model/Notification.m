//
//  Notification.m
//  
//
//  Created by Miguel Martin Nieto on 27/9/15.
//
//

#import "Notification.h"
#import "User.h"


@implementation Notification

@dynamic elementId;
@dynamic type;
@dynamic message;
@dynamic sourceName;
@dynamic sourceId;
@dynamic user;

@end
