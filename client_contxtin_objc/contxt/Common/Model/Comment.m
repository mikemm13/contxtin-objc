//
//  Comment.m
//  
//
//  Created by Miguel Martin Nieto on 14/8/15.
//
//

#import "Comment.h"
#import "Dialogue.h"


@implementation Comment

@dynamic date;
@dynamic message;
@dynamic name;
@dynamic uid;
@dynamic dialogue;

@end
