//
//  ContxtCategory+DashboardItem.h
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "ContxtCategory.h"
#import "DashboardItemProtocol.h"
#import "CategoryItemProtocol.h"

@interface ContxtCategory (DashboardItem)<DashboardItemProtocol, CategoryItemProtocol>

@property (nonatomic, strong) NSString *screenWhereCategoryIsShown;

@end
