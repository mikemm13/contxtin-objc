//
//  Tag.h
//  
//
//  Created by Miguel Martin Nieto on 14/8/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cloud;

@interface Tag : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Cloud *cloud;

@end
