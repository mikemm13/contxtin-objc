//
//  ContxtCategory.h
//  
//
//  Created by Miguel Martin Nieto on 2/9/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cloud, Source, User;

@interface ContxtCategory : NSManagedObject

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSNumber * selected;
@property (nonatomic, retain) NSSet *clouds;
@property (nonatomic, retain) NSSet *sources;
@property (nonatomic, retain) NSSet *users;
@end

@interface ContxtCategory (CoreDataGeneratedAccessors)

- (void)addCloudsObject:(Cloud *)value;
- (void)removeCloudsObject:(Cloud *)value;
- (void)addClouds:(NSSet *)values;
- (void)removeClouds:(NSSet *)values;

- (void)addSourcesObject:(Source *)value;
- (void)removeSourcesObject:(Source *)value;
- (void)addSources:(NSSet *)values;
- (void)removeSources:(NSSet *)values;

- (void)addUsersObject:(User *)value;
- (void)removeUsersObject:(User *)value;
- (void)addUsers:(NSSet *)values;
- (void)removeUsers:(NSSet *)values;

@end
