//
//  Cloud.h
//  
//
//  Created by Miguel Martin Nieto on 15/9/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ContxtCategory, Dialogue, Ranking, Tag;

@interface Cloud : NSManagedObject

@property (nonatomic, retain) NSString * cid;
@property (nonatomic, retain) NSNumber * saved;
@property (nonatomic, retain) ContxtCategory *contxtCategory;
@property (nonatomic, retain) Dialogue *dialogue;
@property (nonatomic, retain) NSSet *rankingElements;
@property (nonatomic, retain) NSSet *tags;
@end

@interface Cloud (CoreDataGeneratedAccessors)

- (void)addRankingElementsObject:(Ranking *)value;
- (void)removeRankingElementsObject:(Ranking *)value;
- (void)addRankingElements:(NSSet *)values;
- (void)removeRankingElements:(NSSet *)values;

- (void)addTagsObject:(Tag *)value;
- (void)removeTagsObject:(Tag *)value;
- (void)addTags:(NSSet *)values;
- (void)removeTags:(NSSet *)values;

@end
