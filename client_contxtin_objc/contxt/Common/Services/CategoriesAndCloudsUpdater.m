//
//  CategoriesAndCloudsUpdater.m
//  contxt
//
//  Created by Miguel Martin Nieto on 19/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "CategoriesAndCloudsUpdater.h"
#import "SessionManager.h"
#import "User.h"
#import "CategoriesInteractor.h"
#import "CloudsInteractor.h"
#import "FriendsInteractor.h"
#import "NotificationsInteractor.h"

@interface CategoriesAndCloudsUpdater ()

@property (strong, nonatomic) SessionManager *sessionManager;
@property (strong, nonatomic) CategoriesInteractor *categoriesInteractor;
@property (strong, nonatomic) CloudsInteractor *cloudsInteractor;
@property (strong, nonatomic) FriendsInteractor *friendsInteractor;
@property (strong, nonatomic) NotificationsInteractor *notificationsInteractor;

@end

@implementation CategoriesAndCloudsUpdater

- (instancetype)initWithSessionManager:(id)sessionManager
{
    self = [super init];
    if (self) {
        _sessionManager = sessionManager;
    }
    return self;
}

- (CategoriesInteractor *)categoriesInteractor{
    if (!_categoriesInteractor) {
        _categoriesInteractor = [[CategoriesInteractor alloc] initWithSessionManager:self.sessionManager];
    }
    return _categoriesInteractor;
}

- (CloudsInteractor *)cloudsInteractor{
    if (!_cloudsInteractor) {
        _cloudsInteractor = [[CloudsInteractor alloc] initWithSessionManager:self.sessionManager];
    }
    return _cloudsInteractor;
}

- (FriendsInteractor *)friendsInteractor {
    if (!_friendsInteractor) {
        _friendsInteractor = [[FriendsInteractor alloc] initWithSessionManager:self.sessionManager];
    }
    return _friendsInteractor;
}

- (NotificationsInteractor *)notificationsInteractor {
    if (!_notificationsInteractor) {
        _notificationsInteractor = [[NotificationsInteractor alloc] initWithSessionManager:self.sessionManager];
    }
    return _notificationsInteractor;
}

- (void)loadCategoriesAndClouds{
    User *userLogged = [self.sessionManager userLogged];
    if (userLogged) {
        [self loadCategories];
        [self loadClouds];
        [self loadFriends];
        [self startRequestingNotifications];
    } else {
        [self waitForLoginAndUpdateCategoriesAndClouds];
    }
}

- (void) loadCategoriesAndCloudsWithNotification:(NSNotification *)notification{
    [self loadCategories];
    [self loadClouds];
    [self loadFriends];
    [self startRequestingNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadCategories{
    [self.categoriesInteractor categoriesItemsWithCompletion:^() {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"categoriesLoaded" object:nil];
        NSLog(@"Categories updated");
    }];
}

- (void) loadClouds{
    [self.cloudsInteractor cloudsWithCompletion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"cloudsUpdated" object:nil];
        NSLog(@"Clouds updated");
    }];
}

- (void) loadFriends {
    [self.friendsInteractor friendsItemsWithCompletion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"friendsUpdated" object:nil];
    }];
}

- (void)waitForLoginAndUpdateCategoriesAndClouds{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadCategoriesAndCloudsWithNotification:) name:@"userLoggedSuccessfully" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadCategoriesAndCloudsWithNotification:) name:@"userSignedUpSuccessfully" object:nil];
    
}

- (void)startRequestingNotifications {
    [self.notificationsInteractor requestNotifications];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:60.0 target:self selector:@selector(checkNotifications) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
}

- (void)checkNotifications {
    [self.notificationsInteractor requestNotifications];
}

@end
