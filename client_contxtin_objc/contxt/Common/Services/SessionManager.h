//
//  SessionManager.h
//  contxt
//
//  Created by Miguel Martin Nieto on 19/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface SessionManager : NSObject

- (User *)userLogged;
- (void)performLoginWithEmail:(NSString *)email andPassword:(NSString *)password success:(void (^)(void))success failure:(void (^)(NSString *message))failure;
- (void) performSignUpWithUsername:(NSString *)username email:(NSString *)email andPassword:(NSString *)password success:(void (^)(void))success failure:(void (^)(NSString *message))failure;
- (void)performLogoutWithSuccessBlock:(void (^)(void))success failure:(void (^)(NSString *message))failure;
@end
