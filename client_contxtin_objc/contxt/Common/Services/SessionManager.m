//
//  SessionManager.m
//  contxt
//
//  Created by Miguel Martin Nieto on 19/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "SessionManager.h"
#import "RequestManager.h"
#import "UserProvider.h"

@interface SessionManager ()

@property (strong, nonatomic) UserProvider *userProvider;

@end

@implementation SessionManager

- (UserProvider *)userProvider{
    if (!_userProvider) {
        _userProvider = [[UserProvider alloc] init];
    }
    return _userProvider;
}

- (User *)userLogged{
    User *user = [self.userProvider loadUser];
    return user;
}


- (void)performLoginWithEmail:(NSString *)email andPassword:(NSString *)password success:(void (^)(void))success failure:(void (^)(NSString *message))failure{
    RequestManager *requestManager = [[RequestManager alloc] init];
    NSDictionary *parameters = @{@"email":email, @"password":password};
    
    [requestManager POST:@"login" parameters:parameters successBlock:^(id data) {
        //Save user data. TODO: save to keychain
        
        NSString *status = [data objectForKey:@"status"];
        if ([status isEqualToString:@"fail"]) {
            NSDictionary *errorDictionary = [data objectForKey:@"error"];
            NSString *errorCode = [errorDictionary objectForKey:@"code"];
            if ([errorCode isEqualToString:@"001"]) {
                failure(@"User does not exist");
            }
            else {
                failure(@"An error has occured");
            }
        } else {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                User *user = [User MR_createEntityInContext:localContext];
                user.email = email;
                user.password = password;
                user.uid = [[data objectForKey:@"data"] valueForKey:@"uid"];
                user.username = [[data objectForKey:@"data"] valueForKey:@"name"];
            } completion:^(BOOL contextDidSave, NSError *error) {
                success();
            }];
        }
    } errorBlock:^(NSError *error) {
        failure([error localizedDescription]);
    }];
}

- (void)performSignUpWithUsername:(NSString *)username email:(NSString *)email andPassword:(NSString *)password success:(void (^)(void))success failure:(void (^)(NSString *))failure {
    RequestManager *requestManager = [[RequestManager alloc] init];
    NSDictionary *parameters = @{@"name":username, @"email":email, @"password":password};
    
    [requestManager POST:@"sign_up" parameters:parameters successBlock:^(id data) {
        NSLog(@"%@",data);
        NSString *status = [data objectForKey:@"status"];
        if ([status isEqualToString:@"fail"]) {
            NSDictionary *errorDictionary = [data objectForKey:@"error"];
            NSString *errorCode = [errorDictionary objectForKey:@"code"];
            if ([errorCode isEqualToString:@"001"]) {
                failure(@"User already exists");
            } else if ([errorCode isEqualToString:@"002"]) {
                failure(@"User name already in use");
            }
            else {
                failure(@"An error has occured");
            }
        } else {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                User *user = [User MR_createEntity];
                user.email = email;
                user.password = password;
                user.uid = [[data objectForKey:@"data"] valueForKey:@"uid"];
                user.username = username;
            } completion:^(BOOL contextDidSave, NSError *error) {
                success();
            }];
        }
    } errorBlock:^(NSError *error) {
        failure([error localizedDescription]);
    }];
}

- (void)performLogoutWithSuccessBlock:(void (^)(void))success failure:(void (^)(NSString *message))failure {
    RequestManager *requestManager = [[RequestManager alloc] init];
    NSDictionary *parameters = @{@"uid":self.userLogged.uid};
    [requestManager POST:@"logout" parameters:parameters successBlock:^(id data) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid=%@", self.userLogged.uid];
        [User MR_deleteAllMatchingPredicate:predicate inContext:[NSManagedObjectContext MR_defaultContext]];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            success();            
        }];
    } errorBlock:^(NSError *error) {
        
    }];
}

@end