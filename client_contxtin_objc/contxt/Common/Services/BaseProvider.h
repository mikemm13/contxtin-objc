//
//  BaseProvider.h
//  contxt
//
//  Created by Miguel Martin Nieto on 18/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestManager.h"

@interface BaseProvider : NSObject
@property (strong,nonatomic) RequestManager *requestManager;
@end
