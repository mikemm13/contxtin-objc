//
//  NetworkService.h
//  contxt
//
//  Created by Miguel Martin Nieto on 17/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^RequestManagerSuccess)(id data);
typedef void (^RequestManagerError)(NSError *error);
typedef void (^RequestSearchSuccess)(NSArray *results);

@interface RequestManager : NSObject
@property (copy,nonatomic) NSString *baseDomain;
- (void)POST:(NSString *)path parameters:(NSDictionary *)parameters successBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock;
@end