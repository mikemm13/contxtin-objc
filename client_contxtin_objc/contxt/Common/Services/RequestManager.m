//
//  NetworkService.m
//  contxt
//
//  Created by Miguel Martin Nieto on 17/07/14.
//  Copyright (c) 2014 contxt.in. All rights reserved.
//

#import "RequestManager.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@implementation RequestManager

- (instancetype)init
{
    self = [super init];
    if (self) {
//        _baseDomain = @"http://54.204.28.78";
//        _baseDomain = @"http://127.0.0.1:5000";
    _baseDomain = @"http://52.10.215.195:5000";
    }
    return self;
}


- (void)POST:(NSString *)path parameters:(NSDictionary *)parameters successBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:@"file://" forHTTPHeaderField:@"Origin"];
    NSString *url = [self.baseDomain stringByAppendingPathComponent:path];
    
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
        successBlock(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        errorBlock(error);
    }];
    
    
}
@end
