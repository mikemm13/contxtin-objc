//
//  NSManagedObject+JSON.h
//  contxt
//
//  Created by Miguel Martin Nieto on 8/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (JSON)

- (NSDictionary *)toDictionary;

@end
