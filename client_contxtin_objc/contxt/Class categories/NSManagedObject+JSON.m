//
//  NSManagedObject+JSON.m
//  contxt
//
//  Created by Miguel Martin Nieto on 8/9/15.
//  Copyright (c) 2015 contxt.in. All rights reserved.
//

#import "NSManagedObject+JSON.h"

@implementation NSManagedObject (JSON)

- (NSDictionary *)toDictionary
{
    NSArray *attributes = [[self.entity attributesByName] allKeys];
    NSDictionary *dict = [self dictionaryWithValuesForKeys:attributes];
    return dict;
}

@end
